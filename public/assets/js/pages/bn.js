$(function () {
    $.fn.datepicker.defaults.language = 'bd';
    $.fn.datepicker.defaults.isBD = true;

    $('.num2bn').each(function () {
        var data = $(this).text();
        var bengali = num2bn(data);
        $(this).text(bengali);
    });

    $('select').each(function () {
        $(this).attr('placeholder', 'নির্বাচন');
    });

    bdNumber();
    // bnkb_for_select2();
});
