<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapitalManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capital_management', function (Blueprint $table) {
            $table->id();
            $table->integer('share_holder_id')->nullable();
            $table->float('amount')->nullable()->unsigned();
            $table->integer('type')->nullable()->comment('"1" => "invest", "2" => "withdraw"');
            $table->date('date')->nullable();
            $table->text('comment')->nullable();
            $table->integer('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capital_management');
    }
}
