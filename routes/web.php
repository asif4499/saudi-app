<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/user/profile/{id}', 'UserController@showUserProfile')->middleware('mydata')->name('showuserprofile');
Route::post('/user/profile/{id}', 'UserController@updateUserProfile')->name('updateuserprofile');
Route::resource('/customer', 'CustomerController');
Route::get('/get-customer-list', 'CustomerController@list')->name('get-customer-list');
Route::post('/customer/import', 'CustomerController@import')->name('customer.import');
Route::resource('/supplier', 'SupplierController');
Route::get('/get-supplier-list', 'SupplierController@list')->name('get-supplier-list');
Route::resource('/employee', 'EmployeeController');
Route::resource('/category', 'CategoryController');
Route::get('/get-category-list', 'CategoryController@list')->name('get-category-list');
Route::resource('/unit', 'UnitController');
Route::get('/get-unit-list', 'UnitController@list')->name('get-unit-list');
Route::resource('/item', 'ItemController');
Route::get('/get-item-list', 'ItemController@list')->name('get-item-list');
Route::get('/pos', 'PosController@index')->name('posindex');
Route::post('/pos', 'PosController@addToCart')->name('posaddtocart');
Route::post('/deletecart', 'PosController@deleteCartItem')->name('deletecartitem');
Route::post('/deleteallcart', 'PosController@deleteAllCartItem')->name('deleteallcartitem');
Route::resource('/orderbook', 'OrderBookController');
Route::get('/get-order-book-list', 'OrderBookController@list')->name('get-order-book-list');
Route::get('/account', 'AccountController@index')->name('account.index');
Route::post('/account', 'AccountController@store')->name('account.store');
Route::post('/account/edit', 'AccountController@edit')->name('account.edit');
Route::delete('/account/{account}', 'AccountController@destroy')->name('account.destroy');
Route::post('/account/update', 'AccountController@update')->name('account.update');
Route::get('/error', 'ErrorController@err404')->name('404error');
Route::post('/pos/invoice', 'PosController@itemSellInvoiceShow')->name('sellInvoice.show');
Route::resource('/designation', 'DesignationController');
Route::resource('/pay-salary', 'PaySalaryController');
Route::resource('/attendance', 'AttendanceController');
Route::get('/get-attendance-list', 'AttendanceController@getAattendance')->name('get-attendance-list');
Route::resource('/daily-in', 'DailyInController');
Route::post('/get-daily-in-list', 'DailyInController@list')->name('get-daily-in-list');
Route::resource('/daily-out', 'DailyOutController');
Route::post('/get-daily-out-list', 'DailyOutController@list')->name('get-daily-out-list');
Route::resource('/share-holder', 'ShareHolderController');
Route::get('/get-share-holder', 'ShareHolderController@list')->name('get-share-holder');
Route::resource('/capital-management', 'CapitalManagementController');
Route::post('/get-capital-management-list', 'CapitalManagementController@list')->name('get-capital-management-list');
Route::resource('/buy', 'BuyController');
Route::get('/get-buy-list', 'BuyController@list')->name('get-buy-list');
Route::resource('/sell', 'SellController');
Route::get('/sell-buy-list', 'SellController@list')->name('sell-buy-list');
Route::post('/get-price', 'OrderBookController@getPrice')->name('get-price');
Route::resource('/stock', 'StockController');
Route::get('/get-stock-list', 'StockController@list')->name('get-stock-list');
Route::post('/get-stock-list-from-item', 'StockController@listFromItem')->name('get-stock-list-from-item');
Route::post('/stock/get-unit', 'StockController@getUnit')->name('stock.get-unit');