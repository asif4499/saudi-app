<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
{{--                    <i class="more-less glyphicon glyphicon-plus"></i>--}}
                    <i class="more-less glyphicon glyphicon-minus"></i>
                    Filter
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                {!! Form::open(['route' => 'employee.store', 'method' => 'post', 'id'=>'filter']) !!}
                <div class="row clearfix">
                    <div class="col-sm-4">
                        {{ Form::radio('rang_or_month', '2', false, array('id'=>'month')) }}
                        {{ Form::label('month', 'Select Month & Year', ['style'=>'font-weight:bold']) }}
                        <div class="form-group">
                            <div class="form-line">
                                {!! Form::text('datepicker', null, ['class' => 'form-control', 'placeholder' => 'Disable Month & Year', 'id' => 'datepicker', 'disabled'=>'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        {{ Form::radio('rang_or_month', '1', false, array('id'=>'rang')) }}
                        {{ Form::label('rang', 'Select Date Rang', ['style'=>'font-weight:bold']) }}
                        <div class="form-group">
                            <div class="form-line">
                                {!! Form::text('datefilter', null, ['class' => 'form-control', 'id'=>'datefilter', 'placeholder' => 'Disable Date Rang', 'disabled'=>'disabled']) !!}
                            </div>
                        </div>
                    </div>
                    <div style="text-align: center" class="col-sm-4">
{{--                        <label for="designation">Salary Period</label>--}}
{{--                        <div class="form-group">--}}
{{--                            <div class="form-line">--}}
                        <br>
                                {!! Form::reset("<-- Reset Options", ['class' => 'btn btn-primary', 'onclick'=>'resetForm()']) !!}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label for="designation">Salary Period</label>
                        <div class="form-group">
                            <div class="form-line">
                                {!! Form::select('salary_period[]', salary_period() ? ['0'=>'All']+salary_period() ?? [''=>''] : [''=>'']+['0'=>'All'], null, ['class' => 'form-control select2', 'multiple'=>'multiple']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="designation">Designation</label>
                        <div class="form-group">
                            <div class="form-line">
                                {!! Form::select('designation[]', designations() ? ['0'=>'All']+designations() ?? [''=>''] : [''=>'']+['0'=>'All'], null, ['class' => 'form-control select2', 'multiple'=>'multiple']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="designation">Employee</label>
                        <div class="form-group">
                            <div class="form-line">
                                {!! Form::select('employee[]', employees() ? ['0'=>'All']+employees() ?? [''=>''] : [''=>'']+['0'=>'All'], null, ['class' => 'form-control select2', 'multiple'=>'multiple']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div style="text-align: center">
                        <button class="btn btn-success" type="submit">Filter</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('css')
    <!-- Date Range Picker Css-->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
@endpush

@push('scripts')
    <!-- Date Range Picker Js -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('input[name="datefilter"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

        });

        //    month and year
        $("#datepicker").datepicker( {
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months",
            autoclose: true,
        });

    </script>


    <script>
        $("#month").click(function () {
            $('#datefilter').attr({disabled: true, placeholder: 'Disable Date Rang'});
            $('#datepicker').attr({disabled: false, placeholder: 'Select Month & Year'});
        });
        $("#rang").click(function () {
            $('#datefilter').attr({disabled: false, placeholder: 'Select Date Rang'});
            $('#datepicker').attr({disabled: true, placeholder: 'Disable Month & Year'});
        });
    </script>

    <script>
        function resetForm() {
            // alert('d');
            $('#filter')[0].reset();
            // document.getElementById("filter").reset();
        }
    </script>
@endpush