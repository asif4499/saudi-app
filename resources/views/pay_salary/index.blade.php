@extends('layouts.pos')
@section('title', 'Salary List')
@section('block-header', 'Salary List')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @include('pay_salary.filter')
            <div class="card">
                <div class="header t-end">
{{--                    <a href="{{route('employee.create')}}" class="btn btn-danger waves-effect m-r-20">Add New Employee</a>--}}
                </div>

                <div class="body">
                    @include('layouts.messages')
                    <div class="table-responsive">
                        <table data-form="deleteForm" class="table table-bordered table-striped table-hover js-basic-example dataTable js-exportable">
                            <thead class="thead-color">
                            <tr>
                                <th>Sl. No.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Designation</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    @include('employees.delete-modal')--}}
@endsection

@push('scripts')
    <script>
        $('.panel-title > a').click(function() {
        $(this).find('i').toggleClass('fa-plus fa-minus')
        .closest('panel').siblings('panel')
        .find('i')
        .removeClass('fa-minus').addClass('fa-plus');
        });
    </script>
@endpush