@extends('layouts.pos')
@section('title', 'Share Holder')
@section('block-header', 'Edit Share Holder')
@section('head')
    <link href="{{asset('inpos/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@endsection
@section('footer')
    <script src="{{asset('inpos/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')

                    {!! Form::model($share_holder, ['method' => 'patch', 'route' => ['share-holder.update', ['share_holder' =>$share_holder->id]]]) !!}
                    @include('share_holders.form')

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('share-holder.index')}}" class="btn btn-danger" type="submit">Back</a>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection