@extends('layouts.pos')
@section('title', 'Share Holder')
@section('block-header', 'Add Share Holder')
@section('head')
    <link href="{{asset('inpos/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@endsection
@section('footer')
    <script src="{{asset('inpos/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'share-holder.store', 'method' => 'post']) !!}
                        @include('share_holders.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('share-holder.index')}}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Create</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection