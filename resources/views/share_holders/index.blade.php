@extends('layouts.pos')
@section('title', 'Share Holders')
@section('block-header', 'Share Holders')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <a href="{{route('share-holder.create')}}" class="btn btn-success">Add New</a>
                </div>

                <div class="body">
                    @include('layouts.messages')
                    <div class="table-responsive">
                        <table id="get-share-holder" class="table table-bordered table-hover">
                            <thead class="thead-color">
                            <tr>
                                <th>Sl. No.</th>
                                <th>Name</th>
{{--                                <th>Code</th>--}}
                                <th>NID</th>
                                <th>Primary Phone No.</th>
                                <th>Secondary Phone No.</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Updated By</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>


        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#get-share-holder').DataTable({
                processing: true,
                serverSide: true,
                // responsive: true,
                // scrollY: "500px",
                ajax: '{{ url('get-share-holder') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'id' },
                    { data: 'name', name: 'name'},
                    // { data: 'code', name: 'code'},
                    { data: 'nid', name: 'nid'},
                    { data: 'phone', name: 'phone'},
                    { data: 'phone_2', name: 'phone_2'},
                    { data: 'email', name: 'email'},
                    { data: 'address', name: 'address'},
                    { data: 'status', name: 'status',
                        render:
                            function(data, type, row) {
                                if (data == 1){
                                    $status = '<a class="badge badge-success text-center">Active</a>';
                                }else {
                                    $status = '<a class="badge badge-danger text-center">Inactive</a>';
                                }
                                return $status;
                            }
                    },
                    { data: 'created_by', name: 'created_by'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'updated_by', name: 'updated_by'},
                    { data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false,
                        render:
                            function(data, type, row) {
                                // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                                return '<div style="text-align:center" class="action-btn">'+
                                    '<a class="btn btn-info btn-sm" href="share-holder/'+row.id+'/edit">Edit</a>'
                            }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data.name;
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                },
                order: [[ 0, "desc" ]],
            });
        });
    </script>
@endpush