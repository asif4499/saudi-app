<div class="row clearfix">
    <div class="col-sm-4">
        <label for="name">Order ID</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('order_id', null, ['class'=>'form-control', 'required'=>'required', 'disabled'=>'disabled']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <p>
            <b>Customer Name (Required)***</b>
        </p>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('customer_id', customers() ? [''=>'']+customers() : [], null, ['class'=>'select2', 'autocomplete'=>'off', 'required'=>'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Item / Product (Required)*** <span style="color: red">[Price: <span id="itemPrice"><b></b></span> BDT]</span></label>

        <div class="form-group">
            <div class="form-line">
                {!! Form::select('item_id', items() ? [''=>'']+items() : [], null, ['class'=>'select2', 'autocomplete'=>'off', 'required'=>'required', 'onchange' => 'getPrice()','id'=>'item_id']) !!}
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <label for="name">Ordered Date (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('ordered_date', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Estimate Delivery Date</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('delivery_ordered_date', null, ['class'=>'form-control', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Ordered Quantity (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('quantity', null, ['class'=>'form-control', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
</div>
<br>
<hr style="background-color: red">
<div class="row clearfix">
    <div class="col-sm-4">
        <label for="name">Sale Quantity (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('sale_quantity', null, ['class'=>'form-control', 'autocomplete'=>'off', 'id'=>'sale_quantity','onkeyup'=>'payableCount()']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Payable Amount</label>
        <div class="form-group">
            <div class="form-line">
                <span style="color: red" id="payable" class="form-control"></span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Due (Based on Recent Sale)</label>
        <div class="form-group">
            <div class="form-line">
                <span style="color: red" id="due" class="form-control"></span>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Received Amount (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('received_amount', null, ['id'=>'paid','class'=>'form-control', 'autocomplete'=>'off','onkeyup'=>'payableCount()']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Date (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('sales_date', isset($buy) ? $buy->date : \Carbon\Carbon::now(), ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($buy))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'disabled'=>'disabled']) !!}
                @endif
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        payableCount()
        function payableCount() {
            var quantity = $("#sale_quantity").val();
            var price = $("#itemPrice").val();
            var paid = $("#paid").val();
            var payable = (quantity * price);
            var due = (payable - paid);

            $("#payable").text(payable);
            $("#due").text(due);
        }
    </script>

    <script type="text/javascript">
        getPrice();
        function getPrice()
        {
            var itemId = $('#item_id').val();

            $.ajax({
                type: "POST",
                url: "{{route('get-price')}}",
                data: {'id' : itemId},
                success: function(data)
                {
                    $("#itemPrice").text(data);
                }
            });

        }

    </script>
@endpush