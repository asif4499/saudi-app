@extends('layouts.pos')
@section('title', 'Order Book')
@section('block-header', 'Add Order')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'orderbook.store', 'method' => 'post']) !!}
                        @include('order_books.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('orderbook.index')}}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Create</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection