@extends('layouts.pos')
@section('title', 'Daily Out')
@section('block-header', 'Daily Out')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header row">
                    <div class="col-md-6">
                        <div class="d-flex justify-content-start">
                            <a href="{{route('orderbook.create')}}" class="btn btn-success">Add New Record</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="d-flex justify-content-end">
                            @include('daily_out.filter')
                        </div>
                    </div>
                </div>

                <div class="body">
                    @include('layouts.messages')
                    <div class="table-responsive">
                        <table id="get-order-book-list" class="table table-bordered table-hover">
                            <thead class="thead-color">
                            <tr>
                                <th>Sl. No.</th>
                                <th>Order ID</th>
                                <th>Customer Name</th>
                                <th>Item / Product</th>
                                <th>Quantity</th>
                                <th>Ordered Date</th>
                                <th>Estimate Delivery Date</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Updated By</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>


        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#get-order-book-list').DataTable({
                processing: true,
                serverSide: true,
                // responsive: true,
                // scrollY: "500px",
                ajax: '{{ url('get-order-book-list') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'id' },
                    { data: 'order_id', name: 'order_id'},
                    { data: 'customer_id', name: 'customer_id'},
                    { data: 'item_id', name: 'item_id'},
                    { data: 'quantity', name: 'quantity'},
                    { data: 'ordered_date', name: 'ordered_date'},
                    { data: 'delivery_ordered_date', name: 'delivery_ordered_date'},
                    { data: 'status', name: 'status',
                        render:
                            function(data, type, row) {
                                if (data == 1){
                                    $status = '<a class="badge badge-success text-center">Active</a>';
                                }else {
                                    $status = '<a class="badge badge-danger text-center">Inactive</a>';
                                }
                                return $status;
                            }
                    },
                    { data: 'created_by', name: 'created_by'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'updated_by', name: 'updated_by'},
                    { data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false,
                        render:
                            function(data, type, row) {
                                // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                                return '<div style="text-align:center" class="action-btn">'+
                                    '<a target="_blank" class="btn btn-info btn-sm" href="orderbook/'+data+'/edit">Update for Sale</a>'
                            }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data.name;
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                },
                order: [[ 0, "desc" ]],
            });
        });
    </script>
@endpush