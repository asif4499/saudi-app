@extends('layouts.pos')
@section('title', 'Sale from Order Book')
@section('block-header', 'Sale from Order Book')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')

                    {!! Form::model($orderBook, ['method' => 'put', 'route' => ['orderbook.update', ['orderbook' =>$orderBook->id]]]) !!}
                    @include('order_books.sale')
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('orderbook.index')}}" class="btn btn-danger" type="submit">Back</a>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection