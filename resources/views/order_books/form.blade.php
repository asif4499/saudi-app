<div class="row clearfix">
    <div class="col-md-6">
        <p>
            <b>Select Customer (Required)***</b>
        </p>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('customer_id', customers() ? [''=>'']+customers() : [], null, ['class'=>'select2', 'autocomplete'=>'off', 'required'=>'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($orderBook))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'disabled'=>'disabled']) !!}
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Ordered Date (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('ordered_date', isset($orderBook) ? $orderBook->date : \Carbon\Carbon::now(), ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Estimate Delivery Date</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('delivery_ordered_date', null, ['class'=>'form-control', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div style="text-align: center">
            <div class="form-group">
                <button id="addRow" type="button" class="btn btn-info">Click to Add New Product / Item</button>
            </div>
        </div>
    </div>
    <div class="col-md-12" id="newRow"></div>
</div>

@push('scripts')
    <script type="text/javascript">
        // add row
        $("#addRow").click(function () {
            var html = '';
            html += '<div id="inputFormRow">';
            html += '<div class="row">';
            html += '<div class="col-md-6">';
            html += '<p>';
            html += '<b>Select Product / Item (Required)***</b>';
            html += '</p>';
            html += '<div class="form-group">';
            html += '<div class="form-line">';
            html += '{!! Form::select('item_id[]', items() ? [''=>'']+items() : [], null, ['id' =>'product', 'class'=>'select2 form-control', 'autocomplete'=>'off', 'required'=>'required']) !!}';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div class="col-md-3">';
            html += '<label for="name">Quantity (Required)***</label>';
            html += '<div class="form-group">';
            html += '<div class="form-line">';
            html += '{!! Form::text('quantity[]', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}';
            html += '</div>';
            html += '</div>';
            html += '</div>';

            html += '<div style="text-align: center" class="col-md-3">';
            html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            // html += '<div id="inputFormRow">';
            // html += '<div class="input-group mb-3">';
            // html += '<input type="text" name="title[]" class="form-control m-input" placeholder="Enter title" autocomplete="off">';
            // html += '<div class="input-group-append">';
            // html += '<button id="removeRow" type="button" class="btn btn-danger">Remove</button>';
            // html += '</div>';
            // html += '</div>';

            $('#newRow').append(html);
            $('.select2').select2({
                placeholder: "Select an option",
                allowClear: true
            });
        });

        // remove row
        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });
    </script>

@endpush