@extends('layouts.pos')
@section('title', 'Daily In')
@section('block-header', 'Add Daily In')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'daily-in.store', 'method' => 'post']) !!}
                        @include('daily_in.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('daily-in.index')}}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Create</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection