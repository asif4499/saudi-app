<div class="row clearfix">
    <div class="col-md-6">
        <label for="name">Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('name', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Amount (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('amount', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Date (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('date', isset($dailyIn) ? $dailyIn->date : \Carbon\Carbon::now(), ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($dailyIn))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'disabled'=>'disabled']) !!}
                @endif
            </div>
        </div>
    </div>
</div>