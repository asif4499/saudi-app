@extends('layouts.pos')
@section('title', 'Daily Out')
@section('block-header', 'Add Daily Out')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'daily-out.store', 'method' => 'post']) !!}
                        @include('daily_out.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('daily-out.index')}}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Create</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection