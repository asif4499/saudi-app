@extends('layouts.pos')
@section('title', 'Employees')
@section('block-header', 'Edit Employee')
@section('breadcrumb')
    <ul class="nav navbar-nav navbar-left n-left">
        <li><a href="{{route('employee.index')}}">Employees</a></li>
        <li><a href="#">&#62;</a></li>
        <li><a href="{{route('employee.index')}}">Manage Designation</a></li>
        <li><a href="#">&#62;</a></li>
        <li><a href="#">Edit</a></li>
    </ul>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::model($employee, ['route' => ['employee.update', $employee->id], 'method' => 'PATCH']) !!}

                        @include('employees.form')

                        <div class="row">
                            <div style="text-align: center">
                                <a href="{{route('employee.index')}}" class="btn btn-primary">Back</a>
                                <button class="btn btn-success" type="submit">Update</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection