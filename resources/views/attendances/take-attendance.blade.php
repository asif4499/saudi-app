@extends('layouts.pos')
@section('title', 'Employees')
@section('block-header', 'Employees')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @include('attendances.filter')
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {{ Form::open(['route' => 'attendance.store', 'method' => 'post']) }}
                        <div class="t-end">
                            <button type="submit" class="btn btn-success waves-effect m-r-20">
                                Submit
                            </button>
                        </div>
                        <hr>
                        <table id="get-attendance" data-form="deleteForm" class="table table-bordered table-striped table-hover">
                            <thead class="thead-color">
                            <tr>
                                <th>SL</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Designation</th>
                                <th>Attendance</th>
                                <th>Entry Time</th>
                                <th>Exit Time</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>


        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#get-attendance').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // scrollY: "500px",
                ajax: '{{ url('get-attendance-list') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'id' },
                    { data: 'name', name: 'name'},
                    { data: 'phone', name: 'phone'},
                    { data: 'designation', name: 'designation'},
                    { data: 'attendance',
                        render:
                            function(data, type, row) {
                                return '<input type="hidden" name="'+data+'_attendance">'+
                                        '<div class="form-check form-check-inline">'+
                                        '<input class="form-check-input" type="radio" name="'+data+'_attendance" id="'+data+'_present" value="1">'+
                                        '<label style="color:#2b982b" class="form-check-label" for="'+data+'_present">Present</label>'+
                                        '</div>'+
                                        '<div class="form-check form-check-inline">'+
                                        '<input class="form-check-input" type="radio" name="'+data+'_attendance" id="'+data+'_absent" value="2">'+
                                        '<label style="color:#f44336" class="form-check-label" for="'+data+'_absent">Absent</label>'+
                                        '</div>'+
                                        '<div class="form-check form-check-inline">'+
                                        '<input class="form-check-input" type="radio" name="'+data+'_attendance" id="'+data+'_leave" value="0">'+
                                        '<label class="form-check-label" for="'+data+'_leave">On Leave</label>'+
                                        '</div>';
                            }
                    },
                    { data: 'entry',
                        render:
                            function(data, type, row) {
                                return '<input type="time" name="'+data+'_entry_time">';
                            }
                    },
                    { data: 'exit',
                        render:
                            function(data, type, row) {
                                return '<input type="time" name="'+data+'_exit_time">';
                            }
                    },
                    { data: 'designation', name: 'designation'},
                    // {data: 'action', name: 'action', orderable: false,
                    //     render:
                    //         function(data, type, row) {
                    //             // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                    //             return '<div style="text-align:center" class="action-btn">'+
                    //                 '<span class="btn btn-primary btn-sm" id="modalView" onclick="journalView('+row.id+')">View</span>'+
                    //                 '<a class="btn btn-info btn-sm" href="deliveries/'+row.id+'/edit">Edit</a>'+
                    //                 '<a class="btn btn-rose btn-link" target="_blank" href="/print/delivery-info/'+row.id+'">Print</a>'+
                    //                 // '<span class="btn btn-danger btn-sm" onclick="deliveryDelete('+row.id+')">Delete</span>'+
                    //                 '</div>';
                    //         }
                    // },
                ],
            });
        });
    </script>
@endpush