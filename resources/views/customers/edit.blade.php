@extends('layouts.pos')
@section('title', 'Customer')
@section('block-header', 'Edit Customer')
@section('head')
    <link href="{{asset('inpos/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@endsection
@section('footer')
    <script src="{{asset('inpos/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')

                    {!! Form::model($customer, ['method' => 'patch', 'route' => ['customer.update', ['customer' =>$customer->id]]]) !!}
                    @include('customers.form')

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('customer.index')}}" class="btn btn-danger" type="submit">Back</a>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection