@extends('layouts.pos')
@section('title', 'Edit Item')
@section('block-header', 'Edit Item')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('inpos/css/component.css')}}" />
    <style>
        #file-1 {
            display: none;
        }
        .inputfile + label{
            font-size: .8rem;
        }
    </style>
@endsection
@section('footer')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script src="{{asset('inpos/js/custom-file-input.js')}}"></script>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
{{--                    <form action="{{route('item.update', $item->id)}}" method="post" enctype="multipart/form-data">--}}
                    {!! Form::model($item, ['method' => 'patch', 'route' => ['item.update', ['item' => encrypt($item->id)]],  'files' => true]) !!}
                        @include('items.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('item.index')}}" class="btn btn-danger" type="submit">Back</a>
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection