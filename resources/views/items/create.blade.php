@extends('layouts.pos')
@section('title', 'Items')
@section('block-header', 'Add New Item')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('inpos/css/component.css')}}" />
    <style>
        #file-1 {
            display: none;
        }
        .inputfile + label{
            font-size: .8rem;
        }
    </style>
    @endsection
@section('footer')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $('#spinnerInput').spinner({'min':0, 'max':99999999});
    </script>
    <script src="{{asset('inpos/js/custom-file-input.js')}}"></script>
@endsection
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'item.store', 'method'=>'POST', 'files' => true]) !!}
                        @include('items.form')
                    <hr>
                    <div class="text-center">
                        <a href="{{route('item.index')}}" class="btn btn-danger" type="submit">Back</a>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection