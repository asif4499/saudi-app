@extends('layouts.pos')
@section('title', 'Items')
@section('block-header', 'Items')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <a href="{{route('item.create')}}" class="btn btn-success">Add New Item</a>
                </div>

                <div class="body">
                    <table id="get-item-list" data-form="deleteForm" class="table table-bordered table-striped table-hover">
                        <thead class="thead-color">
                        <tr>
                            <th>Sl. No.</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Category</th>
                            <th>Purchase Price</th>
                            <th>Retail Price</th>
                            <th>vat</th>
                            <th>Image</th>
                            <th>Status</th>
{{--                            <th>Created By</th>--}}
{{--                            <th>Created At</th>--}}
{{--                            <th>Updated By</th>--}}
{{--                            <th>Updated At</th>--}}
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


{{--    <form style="display: inline;" action="{{route('item.destroy', $item->id)}}" method="post" onsubmit="return confirm('Are you sure you want to delete {{$item->name}}');">--}}
{{--        {{ method_field('DELETE') }}--}}
{{--        @csrf--}}
{{--        <button class="btn btn-danger waves-effect" type="submit">--}}
{{--            <i class="material-icons">delete</i>--}}
{{--        </button>--}}
{{--    </form>--}}
@endsection


@push('scripts')
    <script>


        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#get-item-list').DataTable({
                processing: true,
                serverSide: true,
                // responsive: true,
                // scrollY: "500px",
                ajax: '{{ url('get-item-list') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'id' },
                    { data: 'name', name: 'name'},
                    { data: 'item_code', name: 'item_code'},
                    { data: 'category', name: 'category'},
                    { data: 'purchase_price', name: 'purchase_price'},
                    { data: 'retail_price', name: 'retail_price'},
                    { data: 'vat', name: 'vat'},
                    { data: 'image', name: 'image'},
                    { data: 'status', name: 'status',
                        render:
                            function(data, type, row) {
                                if (data == 1){
                                    $status = '<a class="badge badge-success text-center">Active</a>';
                                }else {
                                    $status = '<a class="badge badge-danger text-center">Inactive</a>';
                                }
                                return $status;
                            }
                    },
                    // { data: 'created_by', name: 'created_by'},
                    // { data: 'created_at', name: 'created_at'},
                    // { data: 'updated_by', name: 'updated_by'},
                    // { data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false,
                        render:
                            function(data, type, row) {
                                // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                                return '<div style="text-align:center" class="action-btn">'+
                                    '<a class="btn btn-success btn-sm" href="item/'+data+'">View Details</a>'+
                                    '<a class="btn btn-info btn-sm" href="item/'+data+'/edit">Edit</a>'
                            }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data.name;
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                },
                order: [[ 0, "desc" ]],
            });
        });
    </script>

{{--    <script>--}}
{{--        $(document).ready(function(){--}}
{{--            $(".btn").click(function(){--}}
{{--                $("#deleteModal").modal('show');--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

@endpush