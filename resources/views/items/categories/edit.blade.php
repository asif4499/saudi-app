@extends('layouts.app')
@section('title', 'Item Categories')
@section('block-header', 'Edit Item Category')
@section('brd1', 'Items & Stocks')
@section('brd2', 'Category')
@section('brd3', 'Edit Category')

@section('content')
    <div class="card-body">
        @include('layouts.messages')
        {!! Form::model($category, ['method' => 'patch', 'route' => ['category.update', ['category' =>$category->id]]]) !!}
        @include('items.categories.form')

        <div class="row">
            <div class="col-sm-6">
                <a href="{{route('category.index')}}" class="btn btn-danger" type="submit">Back</a>
                <button class="btn btn-primary" type="submit">Update</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{asset('inpos/plugins/bootstrap-select/css/bootstrap-select.css')}}">
@endpush

@push('js')
    <script>
        var inputBox = document.getElementById('catName');

        inputBox.onkeyup = function(){
            document.getElementById('catSlug').value = inputBox.value.replace(/\s+/g, '-').toLowerCase();
        }
    </script>
@endpush