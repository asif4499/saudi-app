<div class="row clearfix">
    <div class="col-md-6">
        <div class="mb-3 mt-3">
            <label class="form-label" for="catName">Category Name (Required)***</label>
            {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'catName']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="mb-3 mt-3">
            <label class="form-label" for="catSlug">Category Code (Required)***</label>
            {!! Form::text('code', null, ['class'=>'form-control', 'id'=>'catSlug']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="mb-3 mt-3">
            <label class="form-label" for="">Category Description</label>
            {!! Form::text('description', null, ['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="mb-3 mt-3">
            <label class="form-label" for="">Status (Required)***</label>
            @if(isset($category))
                {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'form-control select2']) !!}
            @else
                {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'form-control select2', 'disabled'=>'disabled']) !!}
            @endif
        </div>
    </div>

</div>