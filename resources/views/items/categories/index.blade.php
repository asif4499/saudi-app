@extends('layouts.app')
@section('title', 'Categories')
@section('block-header', 'Categories')
@section('brd1', 'Items & Stocks')
@section('brd2', 'Category')
@section('content')

    <div class="card-body">
        <a href="{{route('category.create')}}" class="btn btn-success">Add New Item Category</a>
    </div>

    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane show active" id="alt-pagination-preview">
                <table id="get-category" data-form="deleteForm" class="table table-bordered table-striped table-hover">
                    <thead class="thead-color">
                    <tr>
                        <th>SL</th>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Created By</th>
                        <th>Created At</th>
                        <th>Updated By</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection


@push('js')
    <script>


        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#get-category').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // scrollY: "500px",
                ajax: '{{ url('get-category-list') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'id' },
                    { data: 'name', name: 'name'},
                    { data: 'code', name: 'code'},
                    { data: 'description', name: 'description'},
                    { data: 'status', name: 'status',
                        render:
                            function(data, type, row) {
                                if (data == 1){
                                    $status = '<a class="badge badge-success text-center">Active</a>';
                                }else {
                                    $status = '<a class="badge badge-danger text-center">Inactive</a>';
                                }
                                return $status;
                            }
                    },
                    { data: 'created_by', name: 'created_by'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'updated_by', name: 'updated_by'},
                    { data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false,
                        render:
                            function(data, type, row) {
                                // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                                return '<div style="text-align:center" class="action-btn">'+
                                    '<a class="btn btn-info btn-sm" href="category/'+data+'/edit"><i class="far fa-edit"></i></a>'
                            }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data.name;
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                },
                order: [[ 0, "desc" ]],
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $(".btn").click(function(){
                $("#deleteModal").modal('show');
            });
        });
    </script>
@endpush