@extends('layouts.pos')
@section('title', 'Items')
@section('block-header', 'Add New Item')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">



                    <div class="row clearfix">
                        <div style="text-align: center" class="col-md-12">
                            <p for="image">
                                @if(isset($data))
                                    <img  id="blah" class="img-responsive" src="{{asset('images/items/'.$data->image)}}" width="250" height="200" alt="" >
                                @else
                                    <img id="blah" class="img-responsive" src="{{asset('images/items/no_image.png')}}" width="250" height="200" alt="" >
                                @endif
                            </p>
                        </div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-md-3">
                            <p for="name"><b>Item Name:</b> {{$data->name}}</p>
                        </div>
                        <div class="col-md-3">
                            <p for="item_code"><b>Item Code:</b> {{$data->item_code}}</p>
                        </div>
                        <div class="col-md-3">
                            @php
                                $categories = [];
                                foreach ($data->categories as $category){
                                    $categories[] = $category->name;
                                }
                                $categories = implode(" || ",$categories);
                            @endphp
                            <p for="item_code"><b>Category:</b> {{$categories}}</p>
                        </div>

                        <div class="col-md-3">
                            <p for="retail_price"><b>Purchase Price:</b> {{$data->purchase_price}}</p>
                        </div>

                        <div class="col-md-3">
                            <p for="retail_price"><b>Retail Price:</b> {{$data->retail_price}}</p>
                        </div>
                        <div class="col-md-3">
                            <p for="vat"><b>Vat:</b> {{$data->vat}}%</p>
                        </div>

                        <div class="col-md-3">
                            <p for="unit_id"><b>Unit:</b> {{$data->unit ? $data->unit->name : 'No unit found for this item'}}</p>
                        </div>

                        <div class="col-md-3">
                            <p for="name"><b>Status:</b> {!! $data->status == 1 ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>' !!}</p>
                        </div>

                        <div class="col-md-3">
                            <p for="name"><b>Created By:</b> {{getUserById($data->created_by)}}</p>
                        </div>

                        <div class="col-md-3">
                            <p for="name"><b>Created At:</b> {{$data->created_at}}</p>
                        </div>

                        <div class="col-md-3">
                            <p for="name"><b>Updated By:</b> {{getUserById($data->updated_by)}}</p>
                        </div>

                        <div class="col-md-3">
                            <p for="name"><b>Updated At:</b> {{$data->updated_at}}</p>
                        </div>
                    </div>
                    <hr>
                    <p class="h5 text-info text-center">Stock Details</p>
                    <table id="get-stock" class="table table-bordered table-striped table-hover">
                        <thead class="thead-color">
                        <tr>
                            <th>SL</th>
                            <th>Quantity</th>
                            <th>Stock Name</th>
                            <th>Stock Date</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th>Updated By</th>
                            <th>Updated At</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        $i = 1;
                        $quantityCount = 0;
                        @endphp
                            @foreach($data->stocks as $stock)
                                @php
                                    $quantityCount += $stock->quantity;
                                @endphp
                                <tr>
                                    <td class="text-center">{{$i++}}</td>
                                    <td class="text-center">{{$stock->quantity}}</td>
                                    <td class="text-center">{{$stock->stock_name}}</td>
                                    <td class="text-center">{{$stock->stock_date}}</td>
                                    <td class="text-center">{{getUserById($stock->created_by)}}</td>
                                    <td class="text-center">{{$stock->created_at}}</td>
                                    <td class="text-center">{{getUserById($stock->updated_by)}}</td>
                                    <td class="text-center">{{$stock->updated_at}}</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td class="text-center"><b>Total Stock:</b></td>
                                    <td class="text-center text-primary"><b>{{$quantityCount}}</b></td>
                                    <td colspan="6"></td>
                                </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>

        $(function() {
            $('#get-stock').DataTable( {
                responsive: true
            } );
            {{--var item_id = {!! json_encode(encrypt($data->id)) !!};--}}
            {{--$.ajaxSetup({--}}
            {{--    headers: {--}}
            {{--        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
            {{--    }--}}
            {{--});--}}
            // $('#get-stock').DataTable({
                // processing: true,
                // serverSide: true,
                // responsive: true,
                // scrollY: "500px",
                {{--ajax: {--}}
                {{--    url: '{{ route('get-stock-list-from-item') }}',--}}
                {{--    data: {'item_id': item_id},--}}
                {{--    method: 'post',--}}
                {{--},--}}
                {{--columns: [--}}
                {{--    { data: 'DT_RowIndex', name: 'id' },--}}
                {{--    { data: 'quantity', name: 'quantity'},--}}
                {{--    { data: 'stock_name', name: 'stock_name'},--}}
                {{--    { data: 'stock_date', name: 'stock_date'},--}}
                {{--    { data: 'created_by', name: 'created_by'},--}}
                {{--    { data: 'created_at', name: 'created_at'},--}}
                {{--    { data: 'updated_by', name: 'updated_by'},--}}
                {{--    { data: 'updated_at', name: 'updated_at'},--}}
                {{--],--}}
                // columnDefs: { className: "text-center", targets: "1" },
                // order: [[ 0, "desc" ]],
            // });
        });



    </script>

@endpush