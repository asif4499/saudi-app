<div class="row clearfix">
    <div class="col-md-3">
        <label for="name">Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('name', null, ['class'=>'form-control', 'autocomplete'=>'off', 'required'=>'required', 'tabindex' => '1']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <label for="item_code">Item Code (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('item_code', null, ['class'=>'form-control', 'autocomplete'=>'off', 'required'=>'required', 'tabindex' => '2']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <label for="item_code">Select Category (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('category[]', categories() ? categories() : [], isset($item) ? $item->categories : null, ['class'=>'select2', 'multiple'=>'multiple', 'autocomplete'=>'off', 'required'=>'required', 'tabindex' => '3']) !!}
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <label for="retail_price">Purchase Price (Required)***</label>
        <div class="input-group spinner" data-trigger="spinner">
            <div class="form-line">
                {{--                                        <input class="form-control text-center" type="number" name="retail_price"  value="0" data-rule="currency">--}}
                {!! Form::number('purchase_price', null, ['class'=>'form-control text-center', 'autocomplete'=>'off', 'required'=>'required', 'tabindex' => '4']) !!}
            </div>
            <span class="input-group-addon">
                <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <label for="retail_price">Retail Price</label>
        <div class="input-group spinner" data-trigger="spinner">
            <div class="form-line">
                {{--                                        <input class="form-control text-center" type="number" name="retail_price"  value="0" data-rule="currency">--}}
                {!! Form::number('retail_price', null, ['class'=>'form-control text-center', 'autocomplete'=>'off', 'tabindex' => '5']) !!}
            </div>
            <span class="input-group-addon">
                <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
            </span>
        </div>
    </div>
    <div class="col-md-3">
        <label for="vat">Vat in % (Required)***</label>
        <div class="input-group spinner" data-trigger="spinner">
            <div class="form-line">
                {{--                                        <input class="form-control text-center" type="number" name="vat"  value="0" data-rule="currency">--}}
                {!! Form::number('vat', 0, ['class'=>'form-control text-center', 'autocomplete'=>'off', 'required'=>'required', 'tabindex' => '6']) !!}
            </div>
            <span class="input-group-addon">
                                        <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                        <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                    </span>
        </div>
    </div>

{{--    <div class="col-md-2">--}}
{{--        <label for="quantity">Quantity (Required)***</label>--}}
{{--        <div class="input-group spinner" data-trigger="spinner">--}}
{{--            <div class="form-line">--}}
{{--                --}}{{--                                        <input id="spinnerInput" class="form-control text-center" type="text" name="quantity"  value="0" data-rule="quantity">--}}
{{--                {!! Form::number('quantity', null, ['class'=>'form-control text-center', 'data-rule'=>'quantity', 'id'=>'spinnerInput', 'autocomplete'=>'off', 'required'=>'required']) !!}--}}
{{--            </div>--}}
{{--            <span class="input-group-addon">--}}
{{--                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>--}}
{{--                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>--}}
{{--                </span>--}}
{{--        </div>--}}
{{--    </div>--}}

        <div class="col-md-3">
            <label for="unit_id">Unit (Required)***</label>
            <div class="form-group">
                <div class="form-line">
                    {!! Form::select('unit_id', units() ? [''=>'']+units() : [], isset($stock) ? $stock->unit_id : null, ['class'=>'select2', 'autocomplete'=>'off', 'required'=>'required']) !!}
                </div>
            </div>
        </div>

    <div class="col-md-3">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($item))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2', 'tabindex' => '7']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'tabindex' => '7']) !!}
                @endif
            </div>
        </div>
    </div>

{{--    <div class="col-md-3">--}}
{{--        <label for="quantity">Quantity (Required)***</label>--}}
{{--        <div class="form-group">--}}
{{--            <div class="form-line">--}}
{{--                {!! Form::number('quantity', null, ['class'=>'form-control', 'id'=>'quantity']) !!}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>

<div class="row clearfix">
    <div style="text-align: center" class="col-md-12">
        <label for="image">Item Image</label>
        <div style="text-align: center" class="box">
            @if(isset($item))
                <img  id="blah" class="img-responsive" src="{{asset('images/items/'.$item->image)}}" width="250" height="200" alt="" >
            @else
                <img id="blah" class="img-responsive" src="{{asset('images/items/no_image.png')}}" width="250" height="200" alt="" >
            @endif
            <br>
            <br>
            <input tabindex="8" accept="image/*" type="file" name="image" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple onchange="readURL(this)"/>
            <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="12" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Choose a file&hellip;</span></label>
        </div>
    </div>
</div>