@extends('layouts.pos')
@section('title', 'Item Stocks')
@section('block-header', 'Item Stocks')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <a href="{{route('stock.create')}}" class="btn btn-success">Add New Stock</a>
                </div>
                <div class="body">
                    <table id="get-stock" data-form="deleteForm" class="table table-bordered table-striped table-hover">
                        <thead class="thead-color">
                        <tr>
                            <th>SL</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Stock Name</th>
                            <th>Stock Date</th>
                            <th>Status</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th>Updated By</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>


        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#get-stock').DataTable({
                processing: true,
                serverSide: true,
                // responsive: true,
                // scrollY: "500px",
                ajax: '{{ route('get-stock-list') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'id' },
                    { data: 'item', name: 'item'},
                    { data: 'quantity', name: 'quantity'},
                    { data: 'unit', name: 'unit'},
                    { data: 'stock_name', name: 'stock_name'},
                    { data: 'stock_date', name: 'stock_date'},
                    { data: 'status', name: 'status',
                        render:
                            function(data, type, row) {
                                if (data == 1){
                                    $status = '<a class="badge badge-success text-center">Active</a>';
                                }else {
                                    $status = '<a class="badge badge-danger text-center">Inactive</a>';
                                }
                                return $status;
                            }
                    },
                    { data: 'created_by', name: 'created_by'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'updated_by', name: 'updated_by'},
                    { data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false,
                        render:
                            function(data, type, row) {
                                // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                                return '<div style="text-align:center" class="action-btn">'+
                                    '<a class="btn btn-info btn-sm" href="stock/'+data+'/edit">Update Stock</a>'
                            }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data.name;
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                },
                order: [[ 0, "desc" ]],
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $(".btn").click(function(){
                $("#deleteModal").modal('show');
            });
        });
    </script>
@endpush