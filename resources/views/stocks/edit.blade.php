@extends('layouts.pos')
@section('title', 'Item Stock')
@section('block-header', 'Edit Item Stock')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::model($stock, ['method' => 'patch', 'route' => ['stock.update', ['stock' =>encrypt($stock->id)]]]) !!}
                        @include('stocks.form')

                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('stock.index')}}" class="btn btn-danger" type="submit">Back</a>
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection