<div class="row clearfix">
    <div class="col-md-4">
        <label for="stock_date">Stock Date (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('stock_date', isset($stock) ? $stock->stock_date : null, ['class'=>'form-control', 'id'=>'stock_date', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="name">Item (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('item_id', items() ? [''=>'']+items() : [], isset($stock) ? $stock->item_id : null, ['class'=>'select2', 'id'=>'item_id', 'autocomplete'=>'off', 'required'=>'required', 'onchange' => 'getUnit()']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="unit_id">Unit</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('', null, ['class'=>'form-control', 'id' => 'unit_id', 'disabled'=>true]) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="quantity">Quantity (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('quantity', null, ['class'=>'form-control', 'id'=>'quantity']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="stock_name">Stock Name (Optional)</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('stock_name', null, ['class'=>'form-control', 'id'=>'stock_name']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($stock))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2']) !!}
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')

    <script>

        getUnit();
        function getUnit(){
            var item_id = $('#item_id').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '{{ route("stock.get-unit") }}',
                data: {'item_id': item_id},
                type: 'post',
                success: function (data) {
                    $("#unit_id").val(data);
                }
            })
        }
    </script>

@endpush