@extends('layouts.pos')
@section('title', 'Item Stocks')
@section('block-header', 'Add Stock')
@section('head')
@endsection


@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'stock.store', 'method' => 'POST']) !!}
                        @include('stocks.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('stock.index')}}" class="btn btn-danger" type="submit">Back</a>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection