@extends('layouts.pos')
@section('title', 'Buy')
@section('block-header', 'Add Buy')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'buy.store', 'method' => 'post']) !!}
                        @include('buy.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('buy.index')}}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Create</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection