<div class="row clearfix">
    <div class="col-md-6">
        <label for="name">Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('name', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <p>
            <b>Select Supplier (Required)***</b>
        </p>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('supplier_id', suppliers() ? [''=>'']+suppliers() : [], null, ['class'=>'select2', 'autocomplete'=>'off', 'required'=>'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">quantity (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('quantity', null, ['id'=>'quantity', 'class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off', 'onkeyup'=>'payableCount()']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">price (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('price', null, ['id'=>'price', 'class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off', 'onkeyup'=>'payableCount()']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <label for="name">Total Payable</label>
        <div class="form-group">
            <div class="form-line">
                <span style="color: red" id="payable" class="form-control"></span>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <label for="name">Total Due</label>
        <div class="form-group">
            <div class="form-line">
                <span style="color: red" id="due" class="form-control"></span>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Amount Paid (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('paid', null, ['id'=>'paid', 'class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off', 'onkeyup'=>'payableCount()']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Date (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('date', isset($buy) ? $buy->date : \Carbon\Carbon::now(), ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($buy))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'disabled'=>'disabled']) !!}
                @endif
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script>
        payableCount()
        function payableCount() {
            var quantity = $("#quantity").val();
            var price = $("#price").val();
            var paid = $("#paid").val();
            var payable = (quantity * price);
            var due = (payable - paid);

            $("#payable").text(payable);
            $("#due").text(due);
        }
    </script>
@endpush