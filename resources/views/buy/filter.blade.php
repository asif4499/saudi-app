<div class="form-group d-flex justify-content-end">
    <div class="form-line">
        {!! Form::text('datefilter', null, ['class' => 'form-control', 'id'=>'date', 'placeholder' => 'Select Date Rang', 'autocomplete'=>'off']) !!}
    </div>
    <div style="margin-left: .5rem" class="form-line">
        {!! Form::select('status', [''=>'', '1' => 'Active', '2'=>'Inactive'], null, ['id'=>'status', 'class'=>'select2']) !!}
    </div>
    <button id="btn_filter" class="btn btn-success" type="submit">Filter</button>
</div>

@push('css')
    <!-- Date Range Picker Css-->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <style>
        .form-group .form-line{
            border-bottom: 1px solid #2E4053;
        }
    </style>
@endpush

@push('scripts')
    <!-- Date Range Picker Js -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $('input[name="datefilter"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

        });

    </script>
@endpush