@extends('layouts.pos')
@section('title', 'Daily Out')
@section('block-header', 'Edit Daily Out')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')

                    {!! Form::model($buy, ['method' => 'patch', 'route' => ['buy.update', ['buy' =>$buy->id]]]) !!}
                    @include('buy.form')

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('buy.index')}}" class="btn btn-danger" type="submit">Back</a>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection