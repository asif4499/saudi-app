<div class="row clearfix">
    <div class="col-md-6">
        <label for="name">Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('name', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Store/Company Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('store_name', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Primary Phone No. (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('phone', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Secondary Phone No.</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('phone_2', null, ['class'=>'form-control', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Market Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('market_name', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Email</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::email('email', null, ['class'=>'form-control', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Address (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('address', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <label for="status">Supplier Type</label>
        <div class="form-group">
            <div class="form-line">
                    {!! Form::select('supplier_type', [''=>'','Dealer' => 'Dealer', 'Company'=>'Company', 'Wholeseller'=>'Wholeseller', 'Distributor'=>'Distributor', 'Instant Sale' =>'Instant Sale'], null, ['class'=>'select2']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($supplier))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'disabled'=>'disabled']) !!}
                @endif
            </div>
        </div>
    </div>
</div>