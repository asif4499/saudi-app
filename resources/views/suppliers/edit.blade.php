@extends('layouts.pos')
@section('title', 'Supplier')
@section('block-header', 'Edit Supplier')
@section('head')
    <link href="{{asset('inpos/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@endsection
@section('footer')
    <script src="{{asset('inpos/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')

                    {!! Form::model($supplier, ['method' => 'patch', 'route' => ['supplier.update', ['supplier' =>$supplier->id]]]) !!}
                    @include('suppliers.form')

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('supplier.index')}}" class="btn btn-danger" type="submit">Back</a>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection