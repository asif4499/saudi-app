<script src="{{asset('assets/libs/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/libs/metismenu/metisMenu.min.js')}}"></script>
<script src="{{asset('assets/libs/simplebar/simplebar.min.js')}}"></script>
<script src="{{asset('assets/libs/node-waves/waves.min.js')}}"></script>

{{--datatable--}}
<script src="//cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
{{--select2--}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<!-- apexcharts js -->
{{--<script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>--}}

<!-- jquery.vectormap map -->
{{--<script src="{{asset('assets/libs/jqvmap/jquery.vmap.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/libs/jqvmap/maps/jquery.vmap.usa.js')}}"></script>--}}

{{--<script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>--}}

<script src="{{asset('assets/js/app.js')}}"></script>

<script>
    $('#dark-sc').change(function() {
        $('#light-sc').css({'display':'block'});
        $('#dark-sc').css({'display':'none'});
        $('body').attr("data-sidebar","dark");
        $("body").removeAttr("data-topbar");
    });
    $('#light-sc').change(function() {
        $('#dark-sc').css({'display':'block'});
        $('#light-sc').css({'display':'none'});
        $('body').attr("data-topbar","colored");
        $("body").removeAttr("data-sidebar");
    });
</script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Select an option',
            allowClear: true
        });
    });
</script>

<script>
    // var path = window.location.pathname;
    // var str = path.split("/");
    // var url = document.location.protocol + "//" + document.location.hostname + "/" + str[1] + "/" + str[2];
    //
    // $('.sub-menu a[href="' + url + '"]').parent('li').addClass('mm-active');
</script>