<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                COPYRIGHT © <script>document.write(new Date().getFullYear())</script> <a target="_blank" href="http://virq.tech/">VirQ Solution.</a>
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    Powered by <i style="color: #FFBF71" class="fas fa-bolt"></i> <a target="_blank" href="http://virq.tech/">VirQ Solution.</a>
                </div>
            </div>
        </div>
    </div>
</footer>