<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="mdi mdi-home-variant-outline"></i><span
                                class="badge rounded-pill bg-primary float-end">3</span>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="calendar.html" class=" waves-effect">
                        <i class="mdi mdi-calendar-outline"></i>
                        <span>Calendar</span>
                    </a>
                </li>

                {{--                    <li class="menu-title">Layouts</li>--}}

                {{--                    <li>--}}
                {{--                        <a href="javascript: void(0);" class="has-arrow waves-effect">--}}
                {{--                            <i class="mdi mdi-email-outline"></i>--}}
                {{--                            <span>Email</span>--}}
                {{--                        </a>--}}
                {{--                        <ul class="sub-menu" aria-expanded="false">--}}
                {{--                            <li><a href="email-inbox.html">Inbox</a></li>--}}
                {{--                            <li><a href="email-read.html">Read Email</a></li>--}}
                {{--                            <li><a href="email-compose.html">Email Compose</a></li>--}}
                {{--                        </ul>--}}
                {{--                    </li>--}}

                {{--                    <li class="menu-title">Capital & Share Holders</li>--}}

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-file-invoice-dollar"></i>
                        <span>Capital & Share</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('share-holder.index')}}">Share Holders</a></li>
                        <li><a href="{{route('capital-management.index')}}">Capital / Withdraw</a></li>
                    </ul>
                </li>

                {{--                    <li class="menu-title">Human Resource Management</li>--}}

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-users-cog"></i>
                        <span>HRM</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('designation.index')}}">Designations</a></li>
                        <li><a href="{{route('employee.index')}}">Employee</a></li>
                        <li><a href="{{route('attendance.create')}}">Take Attendance</a></li>
                        <li><a href="{{route('attendance.index')}}">Attendance List</a></li>
                        <li><a href="{{route('pay-salary.index')}}">Salary List</a></li>
                    </ul>
                </li>

                {{--                    <li class="menu-title">Manage Items & Stocks</li>--}}

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-warehouse"></i>
                        <span>Items & Stocks</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('category.index')}}">Category</a></li>
                        <li><a href="{{route('unit.index')}}">Unit</a></li>
                        <li><a href="{{route('item.index')}}">Items / Products</a></li>
                        <li><a href="{{route('stock.index')}}">Stocks</a></li>
                    </ul>
                </li>

                {{--                    <li class="menu-title">Manage Customers & Suppliers</li>--}}

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-users"></i>
                        <span>Customer & Supplier</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('customer.index')}}">Customers</a></li>
                        <li><a href="{{route('supplier.index')}}">Suppliers</a></li>
                    </ul>
                </li>

                {{--                    <li class="menu-title">Manage Customers & Suppliers</li>--}}

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-money-check-alt"></i>
                        <span>Sales & Order Book</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('orderbook.index')}}">Order Book</a></li>
                        <li><a href="{{route('posindex')}}">Instant Sale</a></li>
                        <li><a href="{{route('sell.index')}}">Sales History</a></li>
                    </ul>
                </li>

                {{--                    <li class="menu-title">Manage Customers & Suppliers</li>--}}

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-shopping-cart"></i>
                        <span>Buy & Cost</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('buy.index')}}">Buy</a></li>
                        <li><a href="{{route('daily-out.index')}}">Daily Out</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->