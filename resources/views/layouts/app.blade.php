<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8" />
    <title>@yield('title') | Enayas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesdesign" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">

    @include('layouts.partials.styles')

    @stack('css')

</head>

<body data-topbar="colored">

<!-- <body data-layout="horizontal" data-topbar="dark"> -->

<!-- Begin page -->
<div id="layout-wrapper">

    @include('layouts.header')

    @include('layouts.sidebar')


    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0">@yield('block-header')</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    @hasSection('brd1')
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">@yield('brd1')</a></li>
                                    @endif
                                    @hasSection('brd2')
                                        <li class="breadcrumb-item active">@yield('brd2')</li>
                                    @endif
                                    @hasSection('brd3')
                                        <li class="breadcrumb-item active">@yield('brd3')</li>
                                    @endif
                                    @hasSection('brd4')
                                        <li class="breadcrumb-item active">@yield('brd4')</li>
                                    @endif
                                    @hasSection('brd5')
                                        <li class="breadcrumb-item active">@yield('brd5')</li>
                                    @endif
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            @yield('content')
                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        @include('layouts.footer')

    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->


<!-- JAVASCRIPT -->
@include('layouts.partials.scripts')

@stack('js')

</body>
</html>
