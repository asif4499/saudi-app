@extends('layouts.pos')
@section('title', 'designations')
@section('block-header', 'Edit designation')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="header">
                    <ol class="breadcrumb">
                        <li><a href="{{route('designation.index')}}">Employees</a></li>
                        <li><a href="{{route('designation.index')}}">Manage Designation</a></li>
                        <li>Edit</li>
                    </ol>
                </div>
                <div class="body">
                    @include('layouts.messages')
                    <form action="{{route('designation.update', $designation->id)}}" method="post">
                        {{method_field('PUT')}}
                        <div class="row clearfix">
                            @csrf
                            <div class="col-sm-6">
                                <label for="title">Designation Name (Required)***</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" placeholder="Enter designation Name" type="text" name="title" value="{{$designation->title}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="code">Code</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" placeholder="Enter code" type="text" name="code" value="{{$designation->code}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="description">Description</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control" placeholder="Enter Description" type="text" name="description" value="{{$designation->description}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="designation">Designation</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control show-tick" name="status" id="status">
                                            <option value="1" {{$designation->status == 1 ? 'selected' : ''}}>Active</option>
                                            <option value="2" {{$designation->status == 2 ? 'selected' : ''}}>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div style="text-align: center">
                                <a href="{{route('designation.index')}}" class="btn btn-primary">Back</a>
                                <button class="btn btn-success" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection