<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effectr" data-dismiss="modal">X</button>
            </div>
            <div class="modal-body">
                <div class="">
                    {!! Form::open(['route' => 'designation.store', 'method' => 'post']) !!}
                    <div class="">
                        <div class="form-group">
                            <label for="title">Designation Name<span class="acc_name_err_message label label-warning"></span></label>
                            <div class="form-line">
                                {!! Form::text('title', old('title'), ['class'=>'form-control', 'id' => 'title', 'placeholder'=>'Designation Name', 'required'=>'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="bank_info">
                        <div class="form-group">
                            <label for="code">Code <span class="bank_name_err_message label label-warning"></span></label>
                            <div class="form-line">
                                {!! Form::text('code', old('code'), ['class'=>'form-control', 'id' => 'code', 'placeholder'=>'Code']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="bank_info">
                        <div class="form-group">
                            <label for="acc_no">Description <span class="acc_no_err_message label label-warning"></span></label>
                            <div class="form-line">
                                {!! Form::text('description', old('description'), ['class'=>'form-control', 'id' => 'description', 'placeholder'=>'Description']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="form-group">
                            <label for="type">Status <span class="type_err_message label label-warning"></span></label>
                            <div class="form-line">
                                <select class="form-control show-tick" name="status" id="status">
                                    <option value="1" selected>Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-warning waves-effect" id="editBtn" style="display: none;">Update Account</button>
                            <button type="submit" class="btn btn-success waves-effect">Create</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>