<div class="modal fade" tabindex="-1" role="dialog" id="confirm" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body t-center">
                <h4 style="color:#f44336;">Are you sure about Delete?</h4>
                <div class="modal-footer t-center">
                    <button type="button" class="btn btn-success btn-lg waves-effectr" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-danger btn-lg waves-effectr" id="delete-btn">Yes</button>
                </div>
            </div>
        </div>
    </div>
</div>