@extends('layouts.pos')
@section('title', 'Accounts')
@section('content')
    <div class="rifat">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header t-end">
                        <button type="button" class="btn btn-danger waves-effect m-r-20" id="addNewBtn" data-toggle="modal" data-target="#largeModal">Add Designation</button>
                    </div>

                    <div class="body">
                        @include('layouts.messages')
                        <div class="messageDiv" style="display: none;transition: all .5s">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="alert alert-success">
                                        <a class="close" href="javascript:void(0);" onclick="closeMessage();"><i class="material-icons">refresh</i></a>
                                        <strong>Success! </strong><span id="message"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table data-form="deleteForm" class="table table-bordered table-striped table-hover js-basic-example dataTable js-exportable">
                                <thead class="thead-color">
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Title</th>
                                    <th>Code</th>
                                    <th>Description</th>
                                    <th>status</th>
                                    <th>Created At</th>
                                    <th>Created By</th>
                                    <th>Updated At</th>
                                    <th>Updated By</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($designations as $designation)
                                    <tr>
                                        <td class="t-center">{{$loop->index+1}}</td>
                                        <td>{{$designation->title}}</td>
                                        <td>{{$designation->code}}</td>
                                        <td>{{$designation->description}}</td>
                                        <td class="t-center">
                                            @if($designation->status == 1)
                                                <span class="btn-success badge">
                                                    Active
                                                </span>
                                            @else
                                                <span class="btn-danger badge">
                                                    Inactive
                                                </span>
                                            @endif
                                        </td>
                                        <td class="t-center">{{$designation->created_at}}</td>
                                        <td class="t-center">
                                            {!! getUserById($designation->created_by) !!}
                                        </td>
                                        <td class="t-center">{{$designation->updated_at}}</td>
                                        <td class="t-center">
                                            {!! getUserById($designation->updated_by) !!}
                                        </td>
                                        <td class="t-center">
                                            <a class="btn btn-primary btn-sm waves-effect" href="{{route('designation.edit',['designation'=>$designation->id])}}" id="editAccountBtn">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                            <form class="form-delete" style="display: inline;" action="{{route('designation.destroy', $designation->id)}}" method="post">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <button data-toggle="modal" data-target=".delete-modal" class="btn btn-danger btn-sm waves-effect" type="button">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('designation.create-modal')
@include('designation.delete-modal')
@endsection
