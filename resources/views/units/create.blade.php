@extends('layouts.pos')
@section('title', 'Item Units')
@section('block-header', 'Add Unit')
@section('head')
@endsection
@section('footer')
    <script>
        var inputBox = document.getElementById('catName');

        inputBox.onkeyup = function(){
            document.getElementById('catSlug').value = inputBox.value.replace(/\s+/g, '-').toLowerCase();
        }
    </script>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'unit.store', 'method' => 'POST']) !!}
                        @include('units.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('unit.index')}}" class="btn btn-danger" type="submit">Back</a>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection