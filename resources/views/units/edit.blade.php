@extends('layouts.pos')
@section('title', 'Item Units')
@section('block-header', 'Edit Item Unit')
@section('head')
    <link href="{{asset('inpos/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
@endsection
@section('footer')
    <script src="{{asset('inpos/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script>
        var inputBox = document.getElementById('catName');

        inputBox.onkeyup = function(){
            document.getElementById('catSlug').value = inputBox.value.replace(/\s+/g, '-').toLowerCase();
        }
    </script>
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::model($unit, ['method' => 'patch', 'route' => ['unit.update', ['unit' =>$unit->id]]]) !!}
                        @include('units.form')

                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('unit.index')}}" class="btn btn-danger" type="submit">Back</a>
                                <button class="btn btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection