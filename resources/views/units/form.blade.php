<div class="row clearfix">
    <div class="col-md-6">
        <label for="name">Unit Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'catName']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Unit Code (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('code', null, ['class'=>'form-control', 'id'=>'catSlug']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Unit Description</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('description', null, ['class'=>'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($unit))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'disabled'=>'disabled']) !!}
                @endif
            </div>
        </div>
    </div>
</div>