@extends('layouts.pos')
@section('title', 'Sell History')
@section('block-header', 'History')
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header row">
                    <div class="col-md-6">
{{--                        <div class="d-flex justify-content-start">--}}
{{--                            <a href="{{route('buy.create')}}" class="btn btn-success">Add New Record</a>--}}
{{--                        </div>--}}
                    </div>
{{--                    <div class="col-md-6">--}}
{{--                        <div class="d-flex justify-content-end">--}}
{{--                            @include('daily_out.filter')--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>

                <div class="body">
                    @include('layouts.messages')
                    <div class="table-responsive">
                        <table id="get-buy-list" class="table table-bordered table-hover">
                            <thead class="thead-color">
                            <tr>
                                <th>Sl. No.</th>
                                <th>Item Name</th>
                                <th>Customer Name</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                                <th>Paid</th>
                                <th>Due</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Created At</th>
                                <th>Updated By</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>


        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#get-sell-list').DataTable({
                processing: true,
                serverSide: true,
                // responsive: true,
                // scrollY: "500px",
                ajax: '{{ url('get-buy-list') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'id' },
                    { data: 'name', name: 'name'},
                    { data: 'supplier_id', name: 'supplier_id'},
                    { data: 'quantity', name: 'quantity'},
                    { data: 'price', name: 'price'},
                    { data: 'total_price', name: 'total_price'},
                    { data: 'paid', name: 'paid'},
                    { data: 'due'},
                    { data: 'date', name: 'date'},
                    { data: 'status', name: 'status',
                        render:
                            function(data, type, row) {
                                if (data == 1){
                                    $status = '<a class="badge badge-success text-center">Active</a>';
                                }else {
                                    $status = '<a class="badge badge-danger text-center">Inactive</a>';
                                }
                                return $status;
                            }
                    },
                    { data: 'created_by', name: 'created_by'},
                    { data: 'created_at', name: 'created_at'},
                    { data: 'updated_by', name: 'updated_by'},
                    { data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false,
                        render:
                            function(data, type, row) {
                                // return '<span class="btn btn-primary" id="modalView" onclick="journalView('+data+')">Details</span>';
                                return '<div style="text-align:center" class="action-btn">'+
                                    '<a target="_blank" class="btn btn-info btn-sm" href="buy/'+row.id+'/edit">Update</a>'
                            }
                    },
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data.name;
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                },
                order: [[ 0, "desc" ]],
            });
        });
    </script>
@endpush