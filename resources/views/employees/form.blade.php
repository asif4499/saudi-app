<div class="row clearfix">
    <div class="col-md-6">
        <label for="name">Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Employee Name', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="email">Email</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Employee Email']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="phone">Phone (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter Employee Phone No.', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="nid">NID</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('nid', null, ['class' => 'form-control', 'placeholder' => 'Enter Employee NID No.']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="present_address">Present Address (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::textarea('present_address', null, ['class' => 'form-control', 'rows' => '4', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="present_address">Permanent Address (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::textarea('permanent_address', null, ['class' => 'form-control', 'rows' => '4', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="designation">Designation (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('designation_id', designations() ? [''=>'']+designations() ?? [''=>''] : [''=>''], null, ['class' => 'form-control select2', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="designation">Salary Period (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('salary_period', salary_period() ? [''=>'']+salary_period() ?? [''=>''] : [''=>''], null, ['class' => 'form-control select2', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="phone">Salary (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('salary', null, ['class' => 'form-control', 'placeholder' => 'Enter Employee Salary', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="status">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('status', ['1'=>'Active']+['2'=>'Inactive'], null, ['class' => 'form-control select2', 'required' => 'required']) !!}
            </div>
        </div>
    </div>
</div>