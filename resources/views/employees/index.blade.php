@extends('layouts.pos')
@section('title', 'Employees')
@section('block-header', 'Employees')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header t-end">
                    <a href="{{route('employee.create')}}" class="btn btn-danger waves-effect m-r-20">Add New Employee</a>
                </div>

                <div class="body">
                    @include('layouts.messages')
                    <div class="table-responsive">
                        <table data-form="deleteForm" class="table table-bordered table-striped table-hover js-basic-example dataTable js-exportable">
                            <thead class="thead-color">
                            <tr>
                                <th>Sl. No.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Designation</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                                <tr>
                                    <td class="t-center">{{$loop->index+1}}</td>
                                    <td class="t-center">{{$employee->name}}</td>
                                    <td class="t-center">{{$employee->phone}}</td>
                                    <td class="t-center">{{$employee->designation->title}}</td>
                                    <td class="t-center">{{$employee->present_address}}</td>
                                    <td class="t-center">
                                        <a class="btn btn-primary waves-effect" href="{{route('employee.edit', $employee->id)}}">
                                            <i class="material-icons">mode_edit</i>
                                        </a>
                                        <form class="form-delete" style="display: inline;" action="{{route('employee.destroy', $employee->id)}}" method="post">
                                            {{ method_field('DELETE') }}
                                            @csrf
                                            <button data-toggle="modal" data-target=".delete-modal" class="btn btn-danger waves-effect" type="submit">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('employees.delete-modal')
@endsection