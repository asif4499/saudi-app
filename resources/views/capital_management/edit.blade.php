@extends('layouts.pos')
@section('title', 'Capital Management')
@section('block-header', 'Edit Capital')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')

                    {!! Form::model($capitalManagement, ['method' => 'patch', 'route' => ['capital-management.update', ['capital_management' =>$capitalManagement->id]]]) !!}
                    @include('capital_management.form')

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('capital-management.index')}}" class="btn btn-danger" type="submit">Back</a>
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection