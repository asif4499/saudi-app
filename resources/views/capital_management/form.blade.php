<div class="row clearfix">
    <div class="col-md-6">
        <label for="name">Name (Required)***</label>
        <div class="form-group">
            <div class="form-line">
{{--                {!! Form::select('share_holder_id', getShareHolders() ? getShareHolders() : [], isset($item) ? $item->categories : null, ['class'=>'select2', 'multiple'=>'multiple', 'autocomplete'=>'off', 'required'=>'required']) !!}--}}
                {!! Form::select('share_holder_id', getShareHolders() ? [''=>'']+getShareHolders() : [], null, ['class'=>'select2', 'autocomplete'=>'off', 'required'=>'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Amount (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::number('amount', null, ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Choose Invest / Withdraw (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::select('type', [''=>'', '1' => 'Invest', '2'=>'Withdraw'], null, ['class'=>'select2', 'required'=>'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Date (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::date('date', isset($capitalManagement) ? $capitalManagement->date : \Carbon\Carbon::now(), ['class'=>'form-control', 'required'=>'required', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Comment</label>
        <div class="form-group">
            <div class="form-line">
                {!! Form::text('comment', null, ['class'=>'form-control', 'autocomplete'=>'off']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <label for="name">Status (Required)***</label>
        <div class="form-group">
            <div class="form-line">
                @if(isset($capitalManagement))
                    {!! Form::select('status', ['1' => 'Active', '2'=>'Inactive'], null, ['class'=>'select2']) !!}
                @else
                    {!! Form::select('status', ['1' => 'Active'], null, ['class'=>'select2', 'disabled'=>'disabled']) !!}
                @endif
            </div>
        </div>
    </div>
</div>