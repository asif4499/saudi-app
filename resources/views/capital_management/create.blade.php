@extends('layouts.pos')
@section('title', 'Capital Management')
@section('block-header', 'Add Capital')

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div class="body">
                    @include('layouts.messages')
                    {!! Form::open(['route' => 'capital-management.store', 'method' => 'post']) !!}
                        @include('capital_management.form')
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{route('capital-management.index')}}" class="btn btn-danger">Back</a>
                                <button class="btn btn-primary" type="submit">Create</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection