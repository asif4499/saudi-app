<?php

namespace App\Http\Controllers;

use App\Http\Requests\DesignationRequest;
use App\Models\designation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations = designation::orderBy('id', 'desc')->get();
        return view('designation.index', ['designations' => $designations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DesignationRequest $request)
    {
        $validatedData = $this->validate($request, [
            'title'  =>  'required|unique:designations',
            'code' =>  'unique:designations|nullable',
            'description' =>  '',
            'status'   =>  'required',
        ]);
        $validatedData['created_by'] = Auth::user()->id;
        $validatedData['updated_at'] = null;
        Designation::create($validatedData);
       return redirect('designation')->with('danger','Data added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show(designation $designation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit(designation $designation)
    {
        return view('designation.edit', ['designation' => $designation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, designation $designation)
    {
        $validatedData = $this->validate($request, [
            'title'  =>  'required|unique:designations,title,'.$designation->id,
            'code' =>  'nullable|unique:designations,code,'.$designation->id,
            'description' =>  '',
            'status'   =>  'required',
        ]);
        $validatedData['updated_by'] = Auth::user()->id;
        Designation::where('id', $designation->id)->update($validatedData);
        return redirect('designation')->with('success','Data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(designation $designation)
    {
        Designation::where('id', $designation->id)->delete();
        return redirect('designation')->with('success','Data deleted successfully');
    }
}
