<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return view('employees.index', ['employees'=>$employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'  =>  'required',
            'email' =>  'nullable|unique:employees',
            'phone' =>  'required|unique:employees',
            'designation_id'   =>  'required',
            'salary_period'   =>  'required',
            'salary'   =>  'required',
            'nid'   =>  'nullable|unique:employees',
            'present_address'   =>  'required',
            'permanent_address'    =>  'required',
            'status'    =>  'required',
        ]);
        $validatedData['created_by'] = Auth::user()->id;
        $validatedData['updated_at'] = null;
        Employee::create($validatedData);
        return redirect()->route('employee.index')->with('success','Employee Inserted Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('employees.edit', ['employee'=>$employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $validatedData = $this->validate($request, [
            'name'  =>  'required',
            'email' =>  'nullable|email|unique:employees,email,'.$employee->id,
            'phone' =>  'required|unique:employees,phone,'.$employee->id,
            'designation_id'   =>  'required',
            'salary_period'   =>  'required',
            'salary'   =>  'required',
            'nid'   =>  'nullable|unique:employees,nid,'.$employee->id,
            'present_address'   =>  'required',
            'permanent_address'    =>  'required',
            'status'    =>  'required',
        ]);
        $validatedData['updated_by'] = Auth::user()->id;
        Employee::find($employee->id)->update($validatedData);
        return redirect()->route('employee.index')->with('success', 'Employee Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delRow = Employee::find($id);
        $delRow->delete();
        return redirect()->route('employee.index')->with('success', 'Employee Deleted Successfully');
    }
}
