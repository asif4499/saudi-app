<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::all();
        return view('suppliers.index', ['suppliers' => $suppliers]);
    }


    public function list(){
        $data = Supplier::select('*');
        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('store_name', function($data){
                return $data->store_name;
            })
            ->editColumn('code', function($data){
                return $data->code;
            })
            ->editColumn('email', function($data){
                return $data->email;
            })
            ->editColumn('phone', function($data){
                return $data->phone;
            })
            ->editColumn('phone_2', function($data){
                return $data->phone_2;
            })
            ->editColumn('supplier_type', function($data){
                return $data->supplier_type;
            })
            ->editColumn('address', function($data){
                return $data->address;
            })
            ->editColumn('market_name', function($data){
                return $data->market_name;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lastId = Supplier::count();
        $newSupplier = $this->validate($request, [
            'name' =>  'required',
            'store_name' =>  'required',
            'email' =>  'nullable|unique:suppliers',
            'phone' =>  'required|unique:suppliers',
            'phone_2' => 'nullable|unique:suppliers',
            'market_name' =>  'required',
            'supplier_type' =>  '',
            'address' =>  'required',
        ]);
        $newSupplier['code'] = 'sup-'.($lastId+1);
        $newSupplier['status'] = 1;
        $newSupplier['created_by'] = Auth::user()->id;
        $newSupplier['updated_at'] = null;
        Supplier::create($newSupplier);
        return redirect()->route('supplier.index')->with('success', 'Supplier Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::find($id);
        return view('suppliers.edit', ['supplier'=>$supplier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateSupplier = Supplier::find($id);
        $updateData = $this->validate($request, [
            'name' =>  'required',
            'store_name' =>  'required',
            'email' =>  'nullable|unique:suppliers,email,'.$id,
            'phone' =>  'required|unique:suppliers,phone,'.$id,
            'phone_2' => 'nullable|unique:suppliers,phone_2,'.$id,
            'market_name' =>  'required',
            'supplier_type' =>  '',
            'address' =>  'required',
            'status' =>  '',
        ]);
        $updateData['updated_by'] = Auth::user()->id;
        $updateSupplier->update($updateData);
        return redirect()->route('supplier.index')->with('success', 'Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delItem = Supplier::find($id);
        $delItem->delete();
        return redirect()->route('supplier.index')->with('success-message', 'Supplier Deleted Successfully');
    }
}
