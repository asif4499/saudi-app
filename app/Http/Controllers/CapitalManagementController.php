<?php

namespace App\Http\Controllers;

use App\Models\CapitalManagement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class CapitalManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = CapitalManagement::all();
        return view('capital_management.index');
    }

    public function list(){
//
        $data = CapitalManagement::select('*');
        return DataTables::of($data)
        ->editColumn('share_holder_id', function($data){
            return getShareHolderNameById($data->share_holder_id);
        })
        ->editColumn('amount', function($data){
            return $data->amount;
        })
        ->editColumn('date', function($data){
            return $data->date;
        })
        ->editColumn('type', function($data){
            return $data->type;
        })
        ->editColumn('comment', function($data){
            return $data->comment;
        })
        ->editColumn('status', function($data){
            return $data->status;
        })
        ->editColumn('created_by', function($data){
            return getUserById($data->created_by);
        })
        ->editColumn('updated_by', function($data){
            $user = getUserById($data->updated_by);
            return $user;
        })
        ->editColumn('created_at', function($data){
            return $data->created_at;
        })
        ->editColumn('updated_at', function($data){
            return $data->updated_at;
        })
        ->addColumn('action', function($data){
            return $data->id;
        })
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('capital_management.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newData = $this->validate($request, [
            'share_holder_id' =>  'required',
            'amount' =>  'required|numeric',
            'type' =>  'required',
            'date' =>  'required',
            'comment' =>  '',
        ]);
        $newData['status'] = 1;
        $newData['created_by'] = Auth::user()->id;
        $newData['updated_at'] = null;
        CapitalManagement::create($newData);
        return redirect()->route('capital-management.index')->with('success', 'New Record Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CapitalManagement  $capitalManagement
     * @return \Illuminate\Http\Response
     */
    public function show(CapitalManagement $capitalManagement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CapitalManagement  $capitalManagement
     * @return \Illuminate\Http\Response
     */
    public function edit(CapitalManagement $capitalManagement)
    {
        return view('capital_management.edit', ['capitalManagement'=>$capitalManagement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CapitalManagement  $capitalManagement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CapitalManagement $capitalManagement)
    {
        $updateCapitalManagement = CapitalManagement::find($capitalManagement->id);
        $updateData = $this->validate($request, [
            'share_holder_id' =>  'required',
            'amount' =>  'required|numeric',
            'type' =>  'required',
            'date' =>  'required',
            'comment' =>  '',
            'status' =>  '',
        ]);
        $updateData['updated_by'] = Auth::user()->id;
        $updateCapitalManagement->update($updateData);
        return redirect()->route('capital-management.index')->with('success', 'Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CapitalManagement  $capitalManagement
     * @return \Illuminate\Http\Response
     */
    public function destroy(CapitalManagement $capitalManagement)
    {
        //
    }
}
