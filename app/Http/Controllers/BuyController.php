<?php

namespace App\Http\Controllers;

use App\Models\Buy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class BuyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('buy.index');
    }

    public function list(Request $request){
        $data = Buy::select('*');
        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('supplier_id', function($data){
                return getSupplierName($data->supplier_id);
            })
            ->editColumn('quantity', function($data){
                return $data->quantity;
            })
            ->editColumn('price', function($data){
                return $data->price;
            })
            ->addColumn('total_price', function($data){
                return ($data->price * $data->quantity);
            })
            ->addColumn('due', function($data){
                $total = ($data->price * $data->quantity);
                return ($total - $data->paid);
            })
            ->editColumn('paid', function($data){
                return $data->paid;
            })
            ->editColumn('date', function($data){
                return $data->date;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        return view('buy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newData = $this->validate($request, [
            'name' =>  'required',
            'supplier_id' =>  'required',
            'quantity' =>  'required',
            'price' =>  'required',
            'paid' =>  'required',
            'date' =>  'required',
        ]);
        $newData['status'] = 1;
        $newData['created_by'] = Auth::user()->id;
        $newData['updated_at'] = null;
        Buy::create($newData);
        return redirect()->route('buy.index')->with('success', 'New Record Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function show(Buy $buy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function edit(Buy $buy)
    {
        return view('buy.edit', compact('buy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buy $buy)
    {
        $updateBuy = Buy::find($buy->id);
        $updateData = $this->validate($request, [
            'name' =>  'required',
            'supplier_id' =>  'required',
            'quantity' =>  'required',
            'price' =>  'required',
            'paid' =>  'required',
            'date' =>  'required',
            'status' =>  'required',
        ]);

        $updateData['updated_by'] = Auth::user()->id;
        $updateBuy->update($updateData);
        return redirect()->route('buy.index')->with('success', 'Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buy $buy)
    {
        //
    }
}
