<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Excel;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class CustomerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $customers = Customer::select('*');
        return view('customers.index');
    }

    public function list(){
        $data = Customer::all();
        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('store_name', function($data){
                return $data->store_name;
            })
            ->editColumn('code', function($data){
                return $data->code;
            })
            ->editColumn('email', function($data){
                return $data->email;
            })
            ->editColumn('phone', function($data){
                return $data->phone;
            })
            ->editColumn('phone_2', function($data){
                return $data->phone_2;
            })
            ->editColumn('customer_type', function($data){
                return $data->customer_type;
            })
            ->editColumn('address', function($data){
                return $data->address;
            })
            ->editColumn('market_name', function($data){
                return $data->market_name;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lastId = Customer::count();
        $newCustomer = $this->validate($request, [
            'name' =>  'required',
            'store_name' =>  'required',
            'email' =>  'nullable|unique:customers',
            'phone' =>  'required|unique:customers',
            'phone_2' => 'nullable|unique:customers',
            'market_name' =>  'required',
            'customer_type' =>  '',
            'address' =>  'required',
        ]);
        $newCustomer['code'] = 'cus-'.($lastId+1);
        $newCustomer['status'] = 1;
        $newCustomer['created_by'] = Auth::user()->id;
        $newCustomer['updated_at'] = null;
        Customer::create($newCustomer);
        return redirect()->route('customer.index')->with('success', 'Customer Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customers.edit', ['customer'=>$customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateSupplier = Customer::find($id);
        $updateData = $this->validate($request, [
            'name' =>  'required',
            'store_name' =>  'required',
            'email' =>  'nullable|unique:customers,email,'.$id,
            'phone' =>  'required|unique:customers,phone,'.$id,
            'phone_2' => 'nullable|unique:customers,phone_2,'.$id,
            'market_name' =>  'required',
            'customer_type' =>  '',
            'address' =>  'required',
            'status' =>  '',
        ]);
        $updateData['updated_by'] = Auth::user()->id;
        $updateSupplier->update($updateData);
        return redirect()->route('customer.index')->with('success', 'Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delCustomer = Customer::find($id);
        $delCustomer->delete();
        return redirect()->route('customer.index')->with('success-message', 'Customer Deleted Successfully');
    }
    public function import(Request $request){
        if($request->hasFile('file')){
            $this->validate($request, [
                'file'  =>  'required|mimes:csv,txt',
            ]);
            if(($handle = fopen($_FILES['file']['tmp_name'], 'r'))!=false){
                fgetcsv($handle);
                while(($data = fgetcsv($handle, 1000, ','))!=false){
                    $list[]= [
                        'name' => $data[0],
                        'email'=> $data[1],
                        'phone'=>$data[2],
                        'address'=>$data[3],
                        'tin_number'=>$data[4],
                        'created_at'=>Carbon::now()->toDateTimeString(),
                        'updated_at'=>Carbon::now()->toDateTimeString(),
                    ];
                }
                if(count($list)>0){
                    Customer::insert($list);
                    return redirect()->route('customer.index')->with('success-message', 'Data uploaded Successfully!');
                }
            }
            return redirect()->route('customer.index')->with('error-message', 'Opps! Something went wrong. Refresh and try again..');
        }
    }
}
