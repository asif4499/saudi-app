<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = null;
        return view('attendances.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::with('today_attendance')->get();
        return view('attendances.take-attendance', compact('employees'));
    }

    public function getAattendance(){
        $data = Employee::select('*')->with('designation');

        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('phone', function($data){
                return $data->phone;
            })
            ->addColumn('designation', function($data){
                return $data->designation->title;
            })
            ->addColumn('attendance', function($data){
                return $data->id;
            })
            ->addColumn('entry', function($data){
                return $data->id;
            })
            ->addColumn('exit', function($data){
                return $data->id;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function store(Request $request)
    {
        $input = array_chunk($request->except(['_token', 'DataTables_Table_0_length', 'get-attendance_length']),3, true);
        foreach ($input as $item) {
            $id = substr(array_keys($item)['0'], 0, strpos(array_keys($item)['0'], '_'));
            $data = [
                'employee_id' => $id,
                'attendance' => $item[$id.'_attendance'],
                'entry_time' => $item[$id.'_entry_time'],
                'exit_time' => $item[$id.'_exit_time'],
                'status' => 1,
                'created_by' => Auth::user()->id,
                'updated_at' => null
            ];
            if ($data['attendance'] == null && $data['entry_time'] == null && $data['exit_time'] == null){
                continue;
            }else{
                if ($data['entry_time'] != null || $data['exit_time'] != null){
                    $data['attendance'] = 1;
                }
                Attendance::create($data);
            }
        }
        return redirect()->back()->with('success', 'Attendance Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }
}
