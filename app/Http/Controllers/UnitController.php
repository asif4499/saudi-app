<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Cartalyst\Converter\Laravel\Facades\Converter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $measurements = Converter::from('weight.lb')->to('weight.kg')->convert(0.9999988107)->format('1,0.00');
//        dd($measurements);
        return view('units.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(){
        $data = Unit::select('*');

        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('code', function($data){
                return $data->code;
            })
            ->editColumn('description', function($data){
                return $data->description;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        return view('units.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'  => 'required|unique:units',
            'code'  =>  'required|unique:units',
            'description'  =>  '',
        ]);
        $validatedData['status'] = 1;
        $validatedData['created_by'] = Auth::user()->id;
        $validatedData['updated_at'] = null;
        Unit::create($validatedData);
        return redirect()->route('unit.index')->with('success', 'Unit Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = Unit::find($id);
        return view('units.edit', ['unit'=>$unit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validate($request, [
            'name'  => 'required|unique:units,name,'.$id,
            'code'  =>  'required|unique:units,code,'.$id,
            'description'  =>  '',
            'status'  =>  '',
        ]);
        $validatedData['updated_by'] = Auth::user()->id;
        $updatingRow = Unit::find($id);
        $updatingRow->update($validatedData);
        return redirect()->route('unit.index')->with('success', 'Unit Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delRow = Unit::find($id);
        $delRow->delete();
        return redirect()->route('unit.index')->with('success-message', 'Data Deleted Successfully');
    }
}
