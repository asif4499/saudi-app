<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Item;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::with('categories')->get();
        return view('items.index', ['items'=>$items]);
    }


    public function list(){
        $data = Item::select('*')->with('categories');
        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('item_code', function($data){
                return $data->item_code;
            })
            ->editColumn('purchase_price', function($data){
                return $data->purchase_price;
            })
            ->editColumn('retail_price', function($data){
                return $data->retail_price;
            })
            ->editColumn('quantity', function($data){
                return $data->quantity;
            })
            ->editColumn('vat', function($data){
                return $data->vat;
            })
            ->editColumn('image', function($data){
                $url= asset('/images/items/'.$data->image);
                return '<img height="100" width="100" src="'.$url.'" alt="">';
            })
            ->editColumn('description', function($data){
                return $data->description;
            })
            ->addColumn('unit', function($data){
                return $data->unit ? $data->unit->name : '';
            })
            ->editColumn('category', function($data){
                $datas = [];
                foreach ($data->categories as $category){
                    $datas[] = $category->name;
                }

                return implode(" || ",$datas);
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return encrypt($data->id);
            })
            ->rawColumns(['image', 'action'])
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('items.create', ['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'  =>  'required|unique:items',
            'item_code'  =>  'required',
            'unit_id'  =>  'required',
            'purchase_price'    =>  'required',
            'retail_price'      =>  '',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'vat'  =>  '',
            'category'  => 'required',
            'status'  => 'required',
        ]);

        $image = $request->file('image');
        if($request->hasFile('image')){
            $uploadedImage = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/items');
            $image->move($destinationPath, $uploadedImage);
            $validatedData['image'] = $uploadedImage;
        }else{
            $validatedData['image'] = null;
        }

        $insertingRow = new Item();
        $insertingRow->name = $validatedData['name'];
        $insertingRow->item_code = $validatedData['item_code'];
        $insertingRow->purchase_price = $validatedData['purchase_price'];
        $insertingRow->retail_price = $validatedData['retail_price'];
        $insertingRow->image = $validatedData['image'];
        $insertingRow->vat = $validatedData['vat'];
        $insertingRow->unit_id = $validatedData['unit_id'];
        $insertingRow->status = 1;
        $insertingRow->created_by = auth()->user()->id;
        $insertingRow->updated_at = null;
        $insertingRow->save();
        $insertingRow->categories()->sync($validatedData['category']);
        return redirect()->route('item.index')->with('success', 'Item Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        $data = Item::find($id);
        return view('items.view-details', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $categories = Category::all();
        $item = Item::with('categories')->where('id', '=', $id)->get()->first();
        if(!$item){
            return redirect()->route('item.index');
        }
        return view('items.edit', ['item'=>$item, 'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $updatingRow = Item::find($id);
        $this->validate($request, [
            'name'  =>  'required|unique:items,name, '.$id,
            'item_code'  =>  'required',
            'unit_id'  =>  'required',
            'purchase_price'    =>  'required',
            'retail_price'      =>  '',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'vat'  =>  '',
            'category'  => 'required',
            'status'  => 'required',
        ]);

        $updatingRow->name = $request->name;
        $updatingRow->item_code = $request->item_code;
        $updatingRow->unit_id = $request->unit_id;
        $updatingRow->purchase_price = $request->purchase_price;
        $updatingRow->retail_price = $request->retail_price;
        $updatingRow->vat = $request->vat;

        $updatingRow->status = $request->status;
        $updatingRow->updated_by = auth()->user()->id;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $uploadedImage = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('images/items');
            if ($updatingRow->image != null){
                unlink($destinationPath.'/'.$updatingRow->image);
            }
            $image->move($destinationPath, $uploadedImage);
            $updatingRow['image'] = $uploadedImage;
        }
        $updatingRow->update();
        $updatingRow->categories()->sync($request->category);
        return redirect()->route('item.index')->with('success', 'Item Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);
        $delRow = Item::find($id);
        $delRow->delete();
        return redirect()->route('item.index')->with('success-message', 'Item Deleted Successfully');
    }
}
