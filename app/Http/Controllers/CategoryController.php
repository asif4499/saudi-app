<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('items.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(){
        $data = Category::select('*');

        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('code', function($data){
                return $data->code;
            })
            ->editColumn('description', function($data){
                return $data->description;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return encrypt($data->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        return view('items.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name'  => 'required|unique:categories',
            'code'  =>  'required|unique:categories',
            'description'  =>  '',
        ]);
        $validatedData['status'] = 1;
        $validatedData['created_by'] = Auth::user()->id;
        $validatedData['updated_at'] = null;
        Category::create($validatedData);
        return redirect()->route('category.index')->with('success', 'Category Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $category = Category::find($id);
        return view('items.categories.edit', ['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validate($request, [
            'name'  => 'required|unique:categories,name,'.$id,
            'code'  =>  'required|unique:categories,code,'.$id,
            'description'  =>  '',
            'status'  =>  '',
        ]);
        $validatedData['updated_by'] = Auth::user()->id;
        $updatingRow = Category::find($id);
        $updatingRow->update($validatedData);
        return redirect()->route('category.index')->with('success', 'Category Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delRow = Category::find($id);
        $delRow->delete();
        return redirect()->route('category.index')->with('success-message', 'Data Deleted Successfully');
    }
}
