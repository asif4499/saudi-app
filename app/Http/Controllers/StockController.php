<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('stocks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(){
        $data = Stock::select('*')->orderBy('stock_date', 'desc');

        return DataTables::of($data)
            ->editColumn('stock_name', function($data){
                return $data->stock_name;
            })
            ->editColumn('stock_date', function($data){
                return $data->stock_date;
            })
            ->editColumn('item', function($data){
                return $data->item->name;
            })
            ->editColumn('quantity', function($data){
                return $data->quantity;
            })
            ->editColumn('unit', function($data){
                return $data->item->unit ? $data->item->unit->name : '';
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return encrypt($data->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function listFromItem(Request $request){
        $item_id = decrypt($request->input('item_id'));

        $data = Stock::select('*')->where('item_id', $item_id)->orderBy('stock_date', 'desc');

        return DataTables::of($data)
            ->editColumn('stock_name', function($data){
                return $data->stock_name;
            })
            ->editColumn('stock_date', function($data){
                return $data->stock_date;
            })
            ->editColumn('item', function($data){
                return $data->item->name;
            })
            ->editColumn('quantity', function($data){
                return $data->quantity;
            })
            ->editColumn('unit', function($data){
                return $data->item->unit ? $data->item->unit->name : '';
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        return view('stocks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'stock_date'  => 'required',
            'stock_name'  =>  'nullable|unique:stocks',
            'item_id'  =>  'required',
//            'unit_id'  =>  'required',
            'quantity'  =>  'required',
            'status'  =>  'required',
        ]);
        $validatedData['status'] = 1;
        $validatedData['created_by'] = Auth::user()->id;
        $validatedData['updated_at'] = null;
        Stock::create($validatedData);
        return redirect()->route('stock.index')->with('success', 'Unit Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $stock = Stock::find($id);
        return view('stocks.edit', ['stock'=>$stock]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = decrypt($id);
        $updatingRow = Stock::find($id);
        $this->validate($request, [
            'stock_date'  => 'required',
            'stock_name'  =>  'nullable|unique:stocks,stock_name, '.$id,
            'item_id'  =>  'required',
//            'unit_id'  =>  'required',
            'quantity'  =>  'required',
            'status'  =>  'required',
        ]);

        $updatingRow->stock_date = $request->stock_date;
        $updatingRow->stock_name = $request->stock_name;
        $updatingRow->item_id = $request->item_id;
        $updatingRow->quantity = $request->quantity;
        $updatingRow->status = $request->status;

        $updatingRow->updated_by = auth()->user()->id;

        $updatingRow->update();

        return redirect()->route('stock.index')->with('success', 'Stock Updated Successfully');
    }

    public function getUnit(Request $request){
        $data = Item::find($request->item_id);
        return $data->unit ? $data->unit->name : 'No unit found for this item';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }
}
