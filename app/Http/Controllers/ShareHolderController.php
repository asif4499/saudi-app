<?php

namespace App\Http\Controllers;

use App\Models\ShareHolder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ShareHolderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $share_holders = ShareHolder::all();
        return view('share_holders.index', ['share_holders' => $share_holders]);
    }

    public function list(){
        $data = ShareHolder::select('*');
        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('code', function($data){
                return $data->code;
            })
            ->editColumn('nid', function($data){
                return $data->nid;
            })
            ->editColumn('email', function($data){
                return $data->email;
            })
            ->editColumn('phone', function($data){
                return $data->phone;
            })
            ->editColumn('phone_2', function($data){
                return $data->phone_2;
            })
            ->editColumn('address', function($data){
                return $data->address;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('share_holders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lastId = ShareHolder::count();
        $newShare_Holder = $this->validate($request, [
            'name' =>  'required',
            'email' =>  'nullable|unique:share_holders',
            'nid' =>  'required|unique:share_holders',
            'phone' =>  'required|unique:share_holders',
            'phone_2' => 'nullable|unique:share_holders',
            'address' =>  'required',
        ]);
        $newShare_Holder['code'] = 'sh-'.($lastId+1);
        $newShare_Holder['status'] = 1;
        $newShare_Holder['created_by'] = Auth::user()->id;
        $newShare_Holder['updated_at'] = null;
        ShareHolder::create($newShare_Holder);
        return redirect()->route('share-holder.index')->with('success', 'Supplier Added Successfully');
    }

    /**
     * Display the specified resource.
     *
//     * @param  \App\Models\ShareHolder  $shareHolder
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *  @param  int  $id
     *
//     * @param  \App\Models\ShareHolder  $shareHolder
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $share_holder = ShareHolder::find($id);
        return view('share_holders.edit', ['share_holder'=>$share_holder]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *  @param  int  $id
//     * @param  \App\Models\ShareHolder  $shareHolder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateShare_Holder = ShareHolder::find($id);
        $updateData = $this->validate($request, [
            'name' =>  'required',
            'email' =>  'nullable|unique:share_holders,email,'.$id,
            'nid' =>  'required|unique:share_holders,nid,'.$id,
            'phone' =>  'required|unique:share_holders,phone,'.$id,
            'phone_2' => 'nullable|unique:share_holders,phone_2,'.$id,
            'address' =>  'required',
            'status' =>  '',
        ]);
        $updateData['updated_by'] = Auth::user()->id;
        $updateShare_Holder->update($updateData);
        return redirect()->route('share-holder.index')->with('success', 'Data Updated Successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
//     * @param  \App\Models\ShareHolder  $shareHolder
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delItem = ShareHolder::find($id);
        $delItem->delete();
        return redirect()->route('share_holders.index')->with('success-message', 'Supplier Deleted Successfully');
    }
}
