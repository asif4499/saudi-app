<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\OrderBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Services\DataTable;

class OrderBookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order_books.index');
    }

    public function list(Request $request){
        $data = OrderBook::select('*');
        return DataTables::of($data)
            ->editColumn('customer_id', function($data){
                return getCustomerName($data->customer_id);
            })
            ->editColumn('item_id', function($data){
                return itemName($data->item_id);
            })
            ->editColumn('quantity', function($data){
                return $data->quantity;
            })
            ->editColumn('ordered_date', function($data){
                return $data->ordered_date;
            })
            ->editColumn('delivery_ordered_date', function($data){
                return $data->delivery_ordered_date;
            })
            ->editColumn('order_id', function($data){
                return 'ODR-'.$data->order_id;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        return view('order_books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newData = $this->validate($request, [
            'customer_id' =>  'required',
            'ordered_date' =>  'required',
            'delivery_ordered_date' =>  '',
            'item_id' =>  'required',
            'quantity' =>  'required',
        ]);
        $items = $newData['item_id'];
        $quantities = $newData['quantity'];
        $orderId = OrderBook::orderBy('order_id', 'desc')->pluck('order_id')->first();
        for ($i = 0; count($items) > $i; $i++){
            $newData['item_id'] = $items[$i];
            $newData['quantity'] = $quantities[$i];
            $newData['status'] = 1;
            $newData['order_id'] = ($orderId+1);
            $newData['created_by'] = Auth::user()->id;
            $newData['updated_at'] = null;
            OrderBook::create($newData);
        }
        return redirect()->route('orderbook.index')->with('success', 'New Record Added Successfully');
    }


    public function getPrice(Request $request){
        $price = Item::where('id', $request->input('id'))->first();
        return response()->json($price->retail_price);
    }

    public function show(OrderBook $orderBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderBook  $orderBook
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orderBook = OrderBook::find($id);
        $orderBook->order_id = 'ODR-'.$orderBook->order_id;
        return view('order_books.edit', compact('orderBook'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrderBook  $orderBook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderBook $orderBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderBook  $orderBook
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderBook $orderBook)
    {
        //
    }
}
