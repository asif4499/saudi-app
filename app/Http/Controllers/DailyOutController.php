<?php

namespace App\Http\Controllers;

use App\Models\DailyOut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class DailyOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $datas = DailyOut::all();
        return view('daily_out.index');
    }

    public function list(Request $request){
//        $status = $request->input('status');
//        $date = explode(" - ", $request->input('date'));
////        $dateOne = date_create($date['0']);
////        $dateTwo = date_create($date['1']);
////        $date_from = date_format($dateOne,"Y-m-d");
////        $date_to = date_format($dateTwo,"Y-m-d");
//        dd($date['0']);
//        $data = (new DailyIn())->newQuery();
//        $dateToAddOne = date('Y-m-d', strtotime($date_to . " +1 days"));
//
//        if (isset($date_from) && isset($date_to)) {
//            $data->whereBetween('daily_ins.date', [$date_from, $dateToAddOne]);
//        }
//
//        if (isset($date_from) && !isset($date_to)) {
//            $data->whereDate('daily_ins.date', '>=', $date_from);
//        }
//
//        if (isset($date_to) && !isset($date_from)) {
//            $data->whereDate('daily_ins.date', '<=', $date_to);
//        }
////        return $data

        $data = DailyOut::select('*');
        return DataTables::of($data)
            ->editColumn('name', function($data){
                return $data->name;
            })
            ->editColumn('amount', function($data){
                return $data->amount;
            })
            ->editColumn('date', function($data){
                return $data->date;
            })
            ->editColumn('status', function($data){
                return $data->status;
            })
            ->editColumn('created_by', function($data){
                return getUserById($data->created_by);
            })
            ->editColumn('updated_by', function($data){
                $user = getUserById($data->updated_by);
                return $user;
            })
            ->editColumn('created_at', function($data){
                return $data->created_at;
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at;
            })
            ->addColumn('action', function($data){
                return $data->id;
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('daily_out.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newData = $this->validate($request, [
            'name' =>  'required',
            'amount' =>  'required|numeric',
            'date' =>  'required',
        ]);
        $newData['status'] = 1;
        $newData['created_by'] = Auth::user()->id;
        $newData['updated_at'] = null;
        DailyOut::create($newData);
        return redirect()->route('daily-out.index')->with('success', 'New Record Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DailyOut  $dailyOut
     * @return \Illuminate\Http\Response
     */
    public function show(DailyOut $dailyOut)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DailyOut  $dailyOut
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyOut $dailyOut)
    {
        return view('daily_out.edit', ['dailyOut'=>$dailyOut]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DailyOut  $dailyOut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyOut $dailyOut)
    {
        $updateDailyOut = DailyOut::find($dailyOut->id);
        $updateData = $this->validate($request, [
            'name' =>  'required',
            'amount' =>  'required|numeric',
            'date' =>  'required',
            'status' =>  '',
        ]);
        $updateData['updated_by'] = Auth::user()->id;
        $updateDailyOut->update($updateData);
        return redirect()->route('daily-out.index')->with('success', 'Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DailyOut  $dailyOut
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyOut $dailyOut)
    {
        //
    }
}
