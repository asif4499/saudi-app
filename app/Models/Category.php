<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function items(){
        return $this->belongsToMany(Item::class, 'category_item');
    }

    public static function getAllCategoriesByArray()
    {
        try {
            // Validate the value...
            $datas = Category::all()->where('status', 1);
            if (count($datas) > 0){
                foreach ($datas as $item) {
                    $data[$item->id] = $item->name;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }
}
