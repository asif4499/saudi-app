<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];


    public static function getAllCustomersByArray()
    {
        try {
            // Validate the value...
            $datas = Customer::all()->where('status', 1);
            if (count($datas) > 0){
                foreach ($datas as $item) {
                    $data[$item->id] = $item->name.' | '.$item->store_name;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }

    public static function getNameById($user_id)
    {
        try {
            // Validate the value...
            $user = Customer::where('id', $user_id)->first();
            return $user->name.' | '.$user->store_name;
        } catch (\Throwable $e) {
            return false;
        }
    }
}
