<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $guarded = [];


    public function employees(){
        return $this->hasMany(Employee::class, 'designation_id', 'id');
    }

    public static function getAllDesignationsByArray()
    {
        try {
            // Validate the value...
            $designation = Designation::all()->where('status', 1);
            if (count($designation) > 0){
                foreach ($designation as $item) {
                    $data[$item->id] = $item->title;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }
}
