<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShareHolder extends Model
{
    protected $guarded = [];

    public static function getAllShareHoldersByArray()
    {
        try {
            // Validate the value...
            $shareHolders = ShareHolder::all()->where('status', 1);
            if (count($shareHolders) > 0){
                foreach ($shareHolders as $item) {
                    $data[$item->id] = $item->name;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }

    public static function getNameById($user_id)
    {
        try {
            // Validate the value...
            $user = ShareHolder::where('id', $user_id)->pluck('name')->first();
            return $user;
        } catch (\Throwable $e) {
            return false;
        }
    }
}
