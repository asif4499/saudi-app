<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $guarded = [];

    public static function getAllSupplierssByArray()
    {
        try {
            // Validate the value...
            $datas = Supplier::all()->where('status', 1);
            if (count($datas) > 0){
                foreach ($datas as $item) {
                    $data[$item->id] = $item->name.' | '.$item->store_name;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }

    public static function getNameById($user_id)
    {
        try {
            // Validate the value...
            $user = Supplier::where('id', $user_id)->pluck('name')->first();
            return $user;
        } catch (\Throwable $e) {
            return false;
        }
    }
}
