<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryPeriod extends Model
{
    const MONTHLY = 'm';
    const WEEKLY = 'w';
    const DAILY = 'd';
    const HOURLY = 'h';
    const PER_PRODUCTION = 'p';

    public static function getAllSalaryPeriodsByArray(){
        return [
            'm' => 'Monthly',
            'w' => 'Weekly',
            'd' => 'Daily',
            'h' => 'Hourly',
            'p' => 'Per Production',
        ];
    }
}
