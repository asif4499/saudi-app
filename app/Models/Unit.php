<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $guarded = [];

    public function items(){
        return $this->hasMany(Item::class, 'unit_id');
    }

    public static function getAllUnitsByArray()
    {
        try {
            // Validate the value...
            $datas = Unit::where('status', 1)->orderBy('id', 'desc')->get();
            if (count($datas) > 0){
                foreach ($datas as $item) {
                    $data[$item->id] = $item->name;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }
}
