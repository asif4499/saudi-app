<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function categories(){
        return $this->belongsToMany(Category::class, 'category_item');
    }

    public function unit(){
        return $this->belongsTo(Unit::class, 'unit_id', 'id');
    }



    protected $fillable = [
        'name',
        'item_code',
        'purchase_price',
        'retail_price',
        'image',
        'discount',
        'description',
        'unit_id',
        'status_id',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];


    public function stocks(){
        return $this->hasMany(Stock::class, 'item_id', 'id')->orderBy('stock_date', 'desc');
    }

    public static function getAllItemsByArray()
    {
        try {
            // Validate the value...
            $datas = Item::all()->where('status', 1);
            if (count($datas) > 0){
                foreach ($datas as $item) {
                    $data[$item->id] = $item->name;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }

    public static function getNameById($user_id)
    {
        try {
            // Validate the value...
            $user = Item::where('id', $user_id)->pluck('name')->first();
            return $user;
        } catch (\Throwable $e) {
            return false;
        }
    }

}
