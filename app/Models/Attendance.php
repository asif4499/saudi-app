<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $guarded = [];

    public function employee(){
        $this->belongsTo(Employee::class,'employee_id', 'id');
    }
}
