<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];

    public function designation(){
        return $this->belongsTo(Designation::class, 'designation_id', 'id');
    }

    public function attendances(){
        return $this->hasMany(Attendance::class, 'employee_id', 'id');
    }

    public function today_attendance(){
        return $this->hasOne(Attendance::class, 'employee_id', 'id')->whereDate('created_at', Carbon::today());
    }

    public static function getAllEmployeesByArray()
    {
        try {
            // Validate the value...
            $employee = Employee::all()->where('status', 1);
            if (count($employee) > 0){
                foreach ($employee as $item) {
                    $data[$item->id] = $item->name;
                }

                return $data;
            }

        } catch (\Throwable $e) {
            return false;
        }
    }
}
