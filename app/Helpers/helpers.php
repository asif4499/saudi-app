<?php
use App\Models\Designation;
use App\Models\Unit;
use App\Models\User;
use App\Models\SalaryPeriod;
use App\Models\Employee;
use App\Models\Category;
use App\Models\ShareHolder;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\Item;
use Illuminate\Support\Facades\Crypt;

function getUserById($user_id)
{
    return $data = User::getUserById($user_id);
}

function getShareHolderNameById($user_id)
{
    return $data = ShareHolder::getNameById($user_id);
}

function designations(){
    return $data = Designation::getAllDesignationsByArray();
}

function salary_period(){
    return $data = SalaryPeriod::getAllSalaryPeriodsByArray();
}

function employees(){
    return $data = Employee::getAllEmployeesByArray();
}

function categories(){
    return $data = Category::getAllCategoriesByArray();
}

function units(){
    return $data = Unit::getAllUnitsByArray();
}


function items(){
    return $data = Item::getAllItemsByArray();
}

function itemName($id){
    return $data = Item::getNameById($id);
}

function customers(){
    return $data = Customer::getAllCustomersByArray();
}

function suppliers(){
    return $data = Supplier::getAllSupplierssByArray();
}

function getShareHolders(){
    return $data = ShareHolder::getAllShareHoldersByArray();
}

function getCustomerName($id){
    return $data = Customer::getNameById($id);
}

function getSupplierName($id){
    return $data = Supplier::getNameById($id);
}